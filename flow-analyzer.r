library(flowCore)
library(flowWorkspace)
library(openCyto)
library(ggcyto)
library(flowStats)

plotTransform <- function(flowSet, xName, yName){
  trans <- estimateLogicle(flowSet[[1]], c(xName, yName))
  flowSetTrans <- transform(flowSet, trans)
  plot <- autoplot(flowSetTrans, xName, yName)
  return(plot)
}

fcsDir <- "1-21-20/data"


flowSet <- read.flowSet(path = fcsDir)
fsApply(flowSet, each_col, median)

trans <- estimateLogicle(flowSet[[1]], c("FSC-A", "Alexa Fluor 488-A"))
#flowSet <- transform(flowSet, trans)

##plot <- plotTransform(flowSet, "FSC-A", "Alexa Fluor 488-A")
#plot

gateSet <- GatingSet(flowSet)

autoplot(trans %on% flowSet, "FSC-A", "Alexa Fluor 488-A")
cellGate1 <- rectangleGate("Alexa Fluor 488-A"=c(200, Inf), filterId="NonDebris") 
cellGate2 <- polygonGate("FSC-W"=c(20000, 230000, 230000, 20000), "SSC-H"=c(24000, 24000, 230000, 230000))

beadGate <- rectangleGate("FSC-A"=c(90000, 160000), "Alexa Fluor 488-A"=c(1, 250), filterId="Beads")

beadsSet <- Subset(flowSet, beadGate)

autoplot(trans %on% beadsSet, "FSC-A", "Alexa Fluor 488-A")

cellSet <- Subset(flowSet, cellGate1 & cellGate2 & !beadGate)
autoplot(trans %on% cellSet, "FSC-A", "Alexa Fluor 488-A")
summary(cellSet)