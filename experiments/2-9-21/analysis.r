library(dplyr)
library(ggplot2)

df <- read.csv('OD_JFL101.csv', header=TRUE, colClasses=c('Dilution'='character'))
ggplot(df, aes(x=Time, y=OD, group=Dilution)) + geom_point(aes(color=Dilution)) + geom_line(aes(color=Dilution))
ggsave('JFL101.png', width=5, height=3)

df <- read.csv('OD_mNeon-FtsN_JFL101.csv', header=TRUE, colClasses=c('Dilution'='character'))
ggplot(df, aes(x=Time, y=OD, group=Dilution)) + geom_point(aes(color=Dilution)) + geom_line(aes(color=Dilution))
ggsave('mNeon-FtsN_JFL101.png', width=5, height=3)

