library(dplyr)
library(ggplot2)

df <- read.csv('results.csv', header=TRUE)# %>% group_by(Group, Temperature)
boxplot(Cell.Count~Time*Group*Temperature, data=df)
plot(Cell.Count~Sample, data=df)
