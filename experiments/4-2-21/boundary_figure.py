from FlowCytometryTools import FCMeasurement, PolyGate, FCPlate, ThresholdGate, QuadGate
from matplotlib.backends.backend_pdf import PdfPages
from pylab import *
import numpy as np
import os


def printCount(countName, counts):
  print('-----------')
  print(countName)
  for count in counts:
    print(count)

fileNameStart = 'data/'

#PFA Cell Gate:
#cellGate = PolyGate([(1.724e+03, 2.310e+03), (4.317e+03, 1.795e+03), (7.314e+03, 4.371e+03), (9.936e+03, 8.697e+03), (8.662e+03, 9.702e+03), (2.683e+03, 7.216e+03), (-7.423e+01, 4.551e+03)], ('FSC-A', 'SSC-A'), region='in', name='cell')
#doubletGate = ~ PolyGate([(1.574e+03, 7.710e+02), (9.806e+03, 9.393e+03), (9.375e+03, 9.898e+03), (-5.387e+02, 8.341e+02)], ('FSC-A', 'FSC-H'), region='in', name='doublet') & cellGate

cellGate = PolyGate([(4.809e+03, 2.645e+03), (6.995e+03, 4.525e+03), (9.927e+03, 8.659e+03), (8.854e+03, 9.715e+03), (4.531e+03, 6.920e+03), (1.972e+03, 5.079e+03), (3.132e+03, 3.006e+03)], ('FSC-A', 'SSC-A'),region='in', name='cell')

notCellGate = PolyGate([(4.809e+03, 2.645e+03), (6.995e+03, 4.525e+03), (9.927e+03, 8.659e+03), (8.854e+03, 9.715e+03), (4.531e+03, 6.920e+03), (1.972e+03, 5.079e+03), (3.132e+03, 3.006e+03)], ('FSC-A', 'SSC-A'),region='out', name='notCell')


ii = 0
fileNames = ['Specimen_001_Tube_001_001.fcs', 'Specimen_001_Tube_006_006.fcs']
labels = ['normal_cells', 'elongated_cells']
for fileName in fileNames:
  print(fileName)
  sample = FCMeasurement(ID='Test Sample', datafile=(fileNameStart + fileName))
  tsample = sample.transform('hlog', channels=['SSC-W', 'SSC-A', 'SSC-H','FSC-A', 'FSC-H', 'FSC-W'], b=500.0) 
  cells = tsample.gate(cellGate)
  notCells = tsample.gate(notCellGate)
  #print(cells.get_data())
  cells.data.to_csv('figure_'+ labels[ii] + '.csv')
  notCells.data.to_csv('figure_not_'+ labels[ii] + '.csv')

  ax = plt.subplot(1, 2, ii+1)
  tsample.plot(['FSC-A', 'SSC-A'], gates=[cellGate])
  plt.title(labels[ii])
  ax.xaxis.label.set_size(8)
  ax.yaxis.label.set_size(8)
  ii += 1

plt.savefig('figure.png')
