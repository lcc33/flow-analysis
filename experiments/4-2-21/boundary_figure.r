library(flowCore)
#library(flowWorkspace)
library(openCyto)
library(ggcyto)
library(flowStats)

plotTransform <- function(flowSet, xName, yName){
  trans <- estimateLogicle(flowSet[[1]], c(xName, yName))
  flowSetTrans <- transform(flowSet, trans)
  plot <- autoplot(flowSetTrans, xName, yName)
  return(plot)
}

fcsDir <- "data"


cellGate <- PolyGate([(4.809e+03, 2.645e+03), (6.995e+03, 4.525e+03), (9.927e+03, 8.659e+03), (8.854e+03, 9.715e+03), (4.531e+03, 6.920e+03), (1.972e+03, 5.079e+03), (3.132e+03, 3.006e+03)], ('FSC-A', 'SSC-A'),region='in', name='cell')

flowSet <- read.flowSet(path = fcsDir)
fsApply(flowSet, each_col, median)

trans <- estimateLogicle(flowSet[[1]], c("FSC-A", "SSC-A"))
#flowSet <- transform(flowSet, trans)

##plot <- plotTransform(flowSet, "FSC-A", "Alexa Fluor 488-A")
#plot

gateSet <- GatingSet(flowSet)

autoplot(trans %on% flowSet, "FSC-A", "SSC-A")
cellGate1 <- rectangleGate("SSC-A"=c(200, Inf), filterId="NonDebris") 
cellGate2 <- polygonGate("FSC-W"=c(20000, 230000, 230000, 20000), "SSC-H"=c(24000, 24000, 230000, 230000))

beadGate <- rectangleGate("FSC-A"=c(90000, 160000), "SSC-A"=c(1, 250), filterId="Beads")

beadsSet <- Subset(flowSet, beadGate)

autoplot(trans %on% beadsSet, "FSC-A", "SSC-A")

cellSet <- Subset(flowSet, cellGate1 & cellGate2 & !beadGate)
autoplot(trans %on% cellSet, "FSC-A", "SSC-A")
summary(cellSet)