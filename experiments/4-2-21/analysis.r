library(tidyverse)

df <- read.csv('4-2-21.csv')
print(df)
df <- df %>% 
  mutate(Log.Count = log2(Cell.Count)) %>%
  mutate(Log.Dilution = log2(Dilution))


dilutions <- unlist(distinct(df, Dilution))
span <- 2
slope_fit <- numeric(length(dilutions)-span)
for (ii in 1:(length(dilutions)-span)){
  slice <- dilutions[ii:(ii+span)]
  df_slice <- filter(df, Dilution %in% slice)
  model <- lm(Log.Count~Log.Dilution, data=df_slice)
  slope_fit[ii] <- coef(model)[2]
}

print(slope_fit)

distance <- abs(slope_fit+1)
linear_region <- which.min(distance)
slice <- dilutions[linear_region:(linear_region+span)]
df_slice <- filter(df, Dilution %in% slice)
best_model <- coef(lm(Log.Count~Log.Dilution, data=df_slice))
count_min <- min(df_slice$Log.Count)
count_max <- max(df_slice$Log.Count)

df <- df %>%
  mutate(fit_y=Log.Dilution*best_model[2]+best_model[1]) %>%
  mutate(safe_range=((Log.Count>=count_min) & Log.Count<=count_max))

ggplot(data=df, aes(Log.Dilution, Log.Count)) + 
  geom_line(aes(x=Log.Dilution, y=fit_y)) +
  geom_point(aes(color=safe_range))
ggsave('log_fit.png', width=5, height=3)

ggplot(data=df, aes(Dilution, Cell.Count)) + 
  geom_line(aes(x=Log.Dilution, y=fit_y)) +
  geom_point(aes(color=safe_range))
ggsave('linear_fit.png', width=5, height=3)
