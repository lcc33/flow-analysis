library(dplyr)

df <- read.csv('results.csv', header=TRUE)# %>% group_by(Group, Temperature)
boxplot(Cell.Count~Temperature*Group, data=df)
plot(Cell.Count~Sample, data=df)
