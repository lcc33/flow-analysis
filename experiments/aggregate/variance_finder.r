library(dplyr)
library(tibble)
library(ggplot2)
library(lubridate)
library(GGally)
library(car)
library(DataExplorer)

import_data <- function(){
  df1 <- read.csv('aggregate - Experiments.csv', header=TRUE, colClasses=c('Date'='character'))
  df2 <- read.csv('aggregate - Cell Count.csv', header=TRUE)
  df <- left_join(df1, df2)
  df <- normalize_cell_count(df)
  return(df)
}

filter_data <- function(df, excluded){
  df$Date <- as.Date(df$Date, format = "%m/%d/%y")
  for (exclude in excluded){
    df <- filter(df, Experiment != exclude)
  }
  df <- filter(df, Date > as.Date('3/3/21', format='%m/%d/%y'))
  overview <- generate_overview_2(df)
  #experiments_good <- filter(overview, mean_covariance<0.03)$Experiment
  #df <- filter(df, Experiment %in% experiments_good)
  return(df)
}

filter_data_2 <- function(df){
  overview <- generate_overview_2(df)

  experiments_good <- filter(overview, Water.Temperature>41.8, Water.Temperature<42.3)$Experiment
  experiments_good_mc4100 <- filter(overview,  Water.Temperature<43, Cell.Line=='MC4100')$Experiment
  experiments_good <- c( experiments_good, experiments_good_mc4100)
  df <- filter(df, Experiment %in% experiments_good)

  return(df)
}

normalize_cell_count <- function(df){
  df_baseline <- df %>%
    filter(Time==0) %>%
    group_by(Experiment) %>%
    summarise(baseline = mean(Cell.Count))# %>%
  df <- full_join(df, df_baseline) %>% 
    mutate(Cell.Count.Normalized = Cell.Count/baseline)
  return(df)
}


generate_overview_2 <- function(df){
  overview <- read.csv('aggregate - Experiments.csv', header=TRUE, colClasses=c('Date'='character'))
  experiments <- unlist(distinct(df, Experiment))
  overview <- filter(overview, Experiment %in% experiments)# & Cell.Line=='JFL101')
  df_baseline <- df %>%
    filter(Time>=30) %>%
    group_by(Experiment) %>%
    summarise(normalized_increase = mean(Cell.Count.Normalized))
  overview <- left_join(overview, df_baseline, by='Experiment')

  df_baseline <- df %>%
    filter(Time==0) %>%
    group_by(Experiment) %>%
    summarise(average_zero_count = mean(Cell.Count))
  overview <- left_join(overview, df_baseline, by='Experiment')

  df_group <- df %>%
    group_by(Experiment, Time) %>%
    summarise(mean_cell_count = mean(Cell.Count), 
      covariance = sd(Cell.Count)/mean(Cell.Count))
  df_group <- df_group %>%
    group_by(Experiment) %>%
    summarise(mean_covariance=mean(covariance))
  overview <- left_join(overview, df_group, by='Experiment')
  
  return(overview)
}



df <- import_data()
df <- filter(df, Experiment != 'I' & Cell.Line=='JFL101')
overview <- generate_overview_2(df)
glimpse(overview)
glimpse(df)

df_group <- df %>% 
  group_by(Experiment, Time) %>%
  summarise(count_norm_sd_time = sd(Cell.Count)/mean(Cell.Count))

df_group <- df_group %>%
  group_by(Experiment) %>%
  summarise(count_norm_sd = mean(count_norm_sd_time))


df_experiment <- read.csv('aggregate - Experiments.csv', header=TRUE, colClasses=c('Date'='character'))
df_experiment <- filter(df_experiment, Experiment != 'I' & Cell.Line=='JFL101')
df_experiment <- left_join(df_experiment, df_group, by='Experiment')

overview_select <- select(overview, OD, LB.Temperature, Overnight.Dilution, mean_covariance, average_zero_count, normalized_increase)

model <- aov(normalized_increase~Overnight.Dilution + LB.Temperature + average_zero_count , data=overview)
Anova(model, type="III")

ggplot(overview) +
  geom_point(aes(x=average_zero_count, y=mean_covariance))

model <- aov(mean_covariance~Overnight.Dilution + LB.Temperature + average_zero_count , data=overview)
Anova(model, type="III")

model <- aov(Overnight.Dilution ~ OD, data=overview)
summary(model)

plot_correlation(data=overview_select)

ggplot(overview) +
  geom_point(aes(x=average_zero_count, y=mean_covariance))

