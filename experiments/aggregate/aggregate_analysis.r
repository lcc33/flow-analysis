library(dplyr)
library(tibble)
library(ggplot2)
library(lubridate)

import_data <- function(){
  df1 <- read.csv('aggregate - Experiments.csv', header=TRUE, colClasses=c('Date'='character'))
  df2 <- read.csv('aggregate - Cell Count.csv', header=TRUE)
  df <- left_join(df1, df2)
  df <- normalize_cell_count(df)
  return(df)
}

filter_data <- function(df, excluded){
  df$Date <- as.Date(df$Date, format = "%m/%d/%y")
  for (exclude in excluded){
    df <- filter(df, Experiment != exclude)
  }
  df <- filter(df, Date > as.Date('3/3/21', format='%m/%d/%y'))
  overview <- generate_overview_2(df)
  #experiments_good <- filter(overview, mean_covariance<0.03)$Experiment
  #df <- filter(df, Experiment %in% experiments_good)
  return(df)
}

filter_data_2 <- function(df){
  overview <- generate_overview_2(df)

  experiments_good <- filter(overview, Water.Temperature>41.8, Water.Temperature<42.3)$Experiment
  experiments_good_mc4100 <- filter(overview,  Water.Temperature<43, Cell.Line=='MC4100')$Experiment
  experiments_good <- c( experiments_good, experiments_good_mc4100)
  df <- filter(df, Experiment %in% experiments_good)

  return(df)
}

normalize_cell_count <- function(df){
  df_baseline <- df %>%
    filter(Time==0) %>%
    group_by(Experiment) %>%
    summarise(baseline = mean(Cell.Count))# %>%
  df <- full_join(df, df_baseline) %>% 
    mutate(Cell.Count.Normalized = Cell.Count/baseline)
  return(df)
}


generate_overview_2 <- function(df){
  overview <- read.csv('aggregate - Experiments.csv', header=TRUE, colClasses=c('Date'='character'))
  experiments <- unlist(distinct(df, Experiment))
  overview <- filter(overview, Experiment %in% experiments)
  
  df_baseline <- df %>%
    filter(Time>30) %>%
    group_by(Experiment) %>%
    summarise(normalized_increase = mean(Cell.Count.Normalized))
  overview <- left_join(overview, df_baseline, by='Experiment')

  df_baseline <- df %>%
    filter(Time==0) %>%
    group_by(Experiment) %>%
    summarise(average_zero_count = mean(Cell.Count))
  overview <- left_join(overview, df_baseline, by='Experiment')

  df_group <- df %>%
    group_by(Experiment, Time) %>%
    summarise(mean_cell_count = mean(Cell.Count), 
      covariance = sd(Cell.Count)/mean(Cell.Count))
  df_group <- df_group %>%
    group_by(Experiment) %>%
    summarise(mean_covariance=mean(covariance))
  overview <- left_join(overview, df_group, by='Experiment')
  
  return(overview)
}



excluded <- c('I', 'L', 'M')
df <- import_data()
df <- filter_data(df, excluded)

df_filtered <- filter_data_2(df)

ggplot(data = df_filtered) + 
  stat_summary(
    mapping = aes(x=Time, y=Cell.Count.Normalized),
    fun.min = function(z) {quantile(z, 0.25)},
    fun.max = function(z) {quantile(z, 0.75)},
    fun = mean
  ) +
  facet_wrap('Cell.Line', scales="free") +
  ylab('Normalized Cell Count') + 
  xlab('Time after Temperature Shift (min.)')
 # xlim(0, 45)
ggsave('results/data_normalized.png', width=8, height=3)

df_jfl101_10min <- df_filtered %>% filter(
  Time==10,
  Cell.Line=='JFL101'
)
print(mean(df_jfl101_10min$Cell.Count.Normalized))

df_mc4100_20min <- df_filtered %>% filter(
  Time==20,
  Cell.Line=='MC4100'
)

print(mean(df_mc4100_20min$Cell.Count.Normalized))