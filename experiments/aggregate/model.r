dt <- 0.1
k <- 1
time_total <- 10
time <- seq(from=0, to=time_total, by=dt)


pre_count <-numeric(length(time))
post_count <- numeric(length(time))
pre_count[1] <- 100*0.05
post_count[1] <- 100*0.95


for (ii in 2:length(time)){
  d_pre_dt <- -k*pre_count[ii-1]*dt
  pre_count[ii] <- d_pre_dt + pre_count[ii-1]
  post_count[ii] <- post_count[ii-1] - 2*d_pre_dt
}
plot(time, post_count+ pre_count)