library(dplyr)

extract_data <- function(file_start){
  time <- c(0, 10, 20, 30, 0, 10, 20, 30)
  temperature <- c(30, 42)
  df <- data.frame()
  for (ii in c(1:length(temperature))){
    file_name <- paste0(file_start, temperature[ii], 'deg.csv')
    print(file_name)
    df_sample <- read.csv(file_name, header=TRUE)
    df_sample$time <- time
    df_sample$temperature <- temperature[ii]
    df <- rbind(df, df_sample)
  }
  return(df)
}

pdf("results_overall.pdf", 7, 10)
par(mfrow=c(2,1))



df <- extract_data('results_redo_')
plot(Cell.Count~time, data=df, col=temperature/10, pch=temperature/10, main="Redo 11-6")
legend('topleft', legend=c(30, 42), col=c(30, 42)/10, pch=c(30, 42)/10)
plot(Doublet.Count~time, data=df, col=temperature/10, pch=temperature/10, main="Redo 11-6")
legend('topleft', legend=c(30, 42), col=c(30, 42)/10, pch=c(30, 42)/10)
plot(Total.Count~time, data=df, col=temperature/10, pch=temperature/10)
legend('topleft', legend=c(30, 42), col=c(30, 42)/10, pch=c(30, 42)/10)


df <- extract_data('results_newdil_')
plot(Cell.Count~time, data=df, col=temperature/10, pch=temperature/10, main="New Dilution")
legend('topleft', legend=c(30, 42), col=c(30, 42)/10, pch=c(30, 42)/10)
plot(Doublet.Count~time, data=df, col=temperature/10, pch=temperature/10, main="New Dilution")
legend('topleft', legend=c(30, 42), col=c(30, 42)/10, pch=c(30, 42)/10)
plot(Total.Count~time, data=df, col=temperature/10, pch=temperature/10)
legend('topleft', legend=c(30, 42), col=c(30, 42)/10, pch=c(30, 42)/10)

dev.off()



