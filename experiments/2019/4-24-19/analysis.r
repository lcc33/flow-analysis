library(matrixStats)
library(plotrix)
library(ggplot2)

read_csv_column <- function(file_name, column_number){
  data <- read.csv(file=file_name, header=TRUE, sep=',')
  column_data <- as.numeric(data[,column_number])
  return(column_data)
}

read_flow_data <- function(column){
  samples <- c("30_deg_a", "30_deg_b", "42_deg_a", "42_deg_b")
  cells <- matrix(nrow=5, ncol=4)

  for (ii in c(1:length(samples))){
    file_name <- paste("results_", samples[ii],".csv",sep='')
    cells[,ii] <- read_csv_column(file_name, column)
  }
  return(cells)
}

read_cfu_data <- function(){
  file_name <- "results_cfu.csv"
  cells_total <- matrix(nrow=5, ncol=4)
  for (ii in c(1:4)){
    cells_total[,ii] <- read_csv_column(file_name, ii+1)
  }
  return(cells_total)
}

plot_error <- function(cells_total, color, time){
  sample_size <- dim(cells_total)[2]
  total_count_avg <- rowMeans(cells_total)
  total_count_stdev <- rowSds(cells_total)
  if (total_count_avg[1]==1){
    total_count_stdev[1] <- 0.01
  }
  total_count_error <- total_count_stdev*1.96/sqrt(sample_size)
  width = 1.5
  xleft <- time-width
  xright <- time+width
  ybottom <- total_count_avg-total_count_error
  ytop <- total_count_avg+total_count_error
  color = paste(color, "50", sep="")
  rect(xleft, ybottom, xright, ytop, col=color, border=NA, density=50)  
  par(new=TRUE)
}

plot_cell_counts <- function(cells_total_30deg, cells_total_42deg, title, sample_names, ylabel){
  sample_size <- dim(cells_total_30deg)[2]
  time <- c(0, 15, 30, 60, 90)
  time_42deg <- time + 0.5
  color_30deg <- "#0577B1"
  color_42deg <- "#C84E00"
  max_value <- max(cells_total_30deg, cells_total_42deg) 
  shapes <- c(0, 1, 2)
  for (ii in c(1:sample_size)){    
    plot(time, cells_total_30deg[,ii], pch=shapes[ii], cex=1.5, col=color_30deg, xlab="Time (min)", ylab=ylabel, main=title, ylim=c(0, max_value))
    par(new=TRUE)
    points(time_42deg, cells_total_42deg[,ii], pch=shapes[ii], col=color_42deg, cex=1.5)
    par(new=TRUE)
  }
  plot_error(cells_total_30deg, color_30deg, time)
  plot_error(cells_total_42deg, color_42deg, time_42deg)
  legend("topleft", c("30 degrees", "42 degrees"), bty="n",pch=15, col=c(color_30deg, color_42deg))
  legend("bottomright", sample_names, bty="n",pch=shapes)
  par(new=FALSE)
}

sum_cells <- function(cells_total){
  cells_30deg <- rowSums(cells_total[,1:2])/sum(cells_total[1,1:2])
  cells_42deg <- rowSums(cells_total[,3:4])/sum(cells_total[1,3:4])
  results <- list("cells_30deg"=cells_30deg, "cells_42deg"=cells_42deg)
  return(results)
}

folder <- "4-24-19/"
pdf("results_overall.pdf", 7, 10)
par(mfrow=c(2,1))
cells_total_flow <- read_flow_data(3) + read_flow_data(4)
cells_total_cfu <- read_cfu_data()

plot_cell_counts(cells_total_flow[,1:2], cells_total_flow[,3:4], "Flow: 4/24/19", c("Sample A", "Sample B"), "Total Cell Count")
plot_cell_counts(cells_total_cfu[,1:2], cells_total_cfu[,3:4], "CFU Counting: 4/24/19", c("Sample A", "Sample B"), "CFU Count")

cells_total_cfu[4, 4] <- cells_total_cfu[4, 3]
cells_single_flow <- read_flow_data(3)
results_flow <- sum_cells(cells_single_flow)
results_cfu <- sum_cells(cells_total_cfu)
cells_30deg <- matrix(0L, nrow=5, ncol=2)
cells_42deg <- matrix(0L, nrow=5, ncol=2)

cells_30deg[,1] <- results_flow$cells_30deg
cells_42deg[,1]<- results_flow$cells_42deg
cells_30deg[,2] <- results_cfu$cells_30deg
cells_42deg[,2]<- results_cfu$cells_42deg

plot_cell_counts(cells_30deg, cells_42deg, "Method Comparison: 4/24/19", c("Flow", "CFU Counting"), "Short Cell Proportion")

dev.off()

