from matplotlib import pyplot as plt
import numpy as np
import csv

def importData(fileName, column):
  data = []
  with open(fileName, 'r') as dataFile:
    dataImport = csv.reader(dataFile, delimiter=',')
    for row in dataImport:
      data.append(row[column])
  dataFile.close()
  data = data[1:]
  for ii in range(len(data)):
    data[ii] = int(data[ii])
  return data

cellCount30 = importData('30deg.csv', 2)
cellCount42 = importData('42deg.csv', 2)
doubletCount30 = importData('30deg.csv', 3)
doubletCount42 = importData('42deg.csv', 3)

totalCells30 = np.add(cellCount30, doubletCount30)
totalCells42 = np.add(cellCount42, doubletCount42)

time = [0, 15, 30, 60, 90]

ax = plt.axes()
plt.plot(time, cellCount30)
plt.plot(time, cellCount42)
plt.plot(time, doubletCount30)
plt.plot(time, doubletCount42)
ax.legend(['30 Degrees', '42 Degrees'])
plt.title('Growth Curve from FlowCytometer')

plt.show()