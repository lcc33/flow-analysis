library(matrixStats)
library(plotrix)
library(ggplot2)

read_csv_column <- function(file_name, column_number){
  data <- read.csv(file=file_name, header=TRUE, sep=',')
  column_data <- as.numeric(data[,column_number])
  return(column_data)
}

read_flow_data <- function(column, samples, time){
  n <- length(samples)  
  cells <- matrix(nrow=length(time), ncol=n)
  for (ii in c(1:n)){
    file_name <- paste("results_", samples[ii],".csv",sep='')
    cells[,ii] <- read_csv_column(file_name, column)
  }
  return(cells)
}

read_cfu_data <- function(folder, time){
  file_name <- paste("results_cfu.csv", sep='')
  cells_total <- matrix(nrow=length(time), ncol=4)
  for (ii in c(1:4)){
    cells_total[,ii] <- read_csv_column(file_name, ii+1)
  }
  return(cells_total)
}

plot_error <- function(cells_total, color, time){
  sample_size <- dim(cells_total)[2]
  total_count_avg <- rowMeans(cells_total)
  total_count_stdev <- rowSds(cells_total)
  if (total_count_avg[1]==1){
    total_count_stdev[1] <- 0.01
  }
  total_count_error <- total_count_stdev*1.96/sqrt(sample_size)
  width = 1.5
  xleft <- time-width
  xright <- time+width
  ybottom <- total_count_avg-total_count_error
  ytop <- total_count_avg+total_count_error
  color = paste(color, "50", sep="")
  rect(xleft, ybottom, xright, ytop, col=color, border=NA, density=50)  
  par(new=TRUE)
}

plot_cell_counts <- function(cells_total_30deg, cells_total_42deg, title, sample_names, ylabel, time){
  sample_size <- dim(cells_total_30deg)[2]
  time_42deg <- time + 0.5
  color_30deg <- "#0577B1"
  color_42deg <- "#C84E00"
  max_value <- max(cells_total_30deg, cells_total_42deg) 
  shapes <- c(0, 1, 2, 3, 4, 5, 6, 7, 8)
  #for (ii in c(1:sample_size)){  
    ii <- 1
    plot(time, cells_total_30deg, pch=15, cex=1.5, col=color_30deg, xlab="Time (min)", ylab=ylabel, main=title, ylim=c(0, max_value))
    par(new=TRUE)
    ii <- 2
    points(time_42deg, cells_total_42deg, pch=15, col=color_42deg, cex=1.5)
    par(new=TRUE)
  #}
  #plot_error(cells_total_30deg, color_30deg, time)
  #plot_error(cells_total_42deg, color_42deg, time_42deg)
  legend("topleft", c("30 degrees", "42 degrees"), bty="n",pch=15, col=c(color_30deg, color_42deg))
 # legend("bottomright", sample_names, bty="n",pch=shapes)
  par(new=FALSE)
}

normalize_t0 <- function(cells){
  for (ii in c(1:dim(cells)[2])){
    cells[,ii] = cells[,ii]/cells[1,ii]
  }
  return(cells)
}

pdf("results_overall.pdf", 7, 10)
par(mfrow=c(2,1))
time <- c(0, 15, 30, 60, 90)
sample_names <- c("30deg", "42deg")
cells <- read_flow_data(3, sample_names, time) + read_flow_data(4, sample_names, time)
title <- "May 15, 2019 Flow"
ylabel <- "Total Cell Count"
plot_cell_counts(cells[,1], cells[,2], title, sample_names, ylabel, time)

cells <- normalize_t0(cells)
ylabel <- "Normalized Cell Count"
plot_cell_counts(cells[,1], cells[,2], title, sample_names, ylabel, time)

dev.off()



