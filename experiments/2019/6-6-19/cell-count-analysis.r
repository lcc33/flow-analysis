library(matrixStats)
library(plotrix)
library(ggplot2)
library(xtable)
library(beeswarm)

read_flow_data <- function(sample_names, fixatives){
  n <- length(sample_names)  
  data <- data.frame()
  for (ii in c(1:n)){
    file_name <- paste("results_", sample_names[ii],".csv",sep='')
    data_sample <- read.csv(file=file_name, header=TRUE, sep=',')
    data_sample$time <- "0min"
    data_sample$sample <- sample_names[ii]
    data_sample$fixative <- fixatives[ii] #
    print(sd(data_sample$Cell.Count))
    data <- rbind(data, data_sample)
  }
  return(data)
}

simulate_samples <- function(data_fixative, mean){
  std <- sd(data_fixative$Cell.Count)
  data_fixative <-do.call("rbind", replicate(repeat_times, data_fixative, simplify=FALSE))
  values <- rnorm(5*repeat_times, mean, std)
  data_fixative$Cell.Count <- values
  return(data_fixative)
}

pdf("results_overall.pdf", 7, 10)
par(mfrow=c(2,1))
time <- c(0, 10, 20, 30)
sample_names <- c("fix_A", "fix_B", "fix_C", "fix_D")
fixatives <- c("Formalin", "Formalde_Glut", "Formaldehyde", "Methanol")


data <- read_flow_data(sample_names, fixatives)
beeswarm(Cell.Count ~ fixative, data = data, pch=16, col=rainbow(4))
beeswarm(Doublet.Count ~ fixative, data = data, pch=16, col=rainbow(4))
beeswarm(FSC.Mean ~ fixative, data = data, pch=16, col=rainbow(4))
beeswarm(SSC.Mean ~ fixative, data = data, pch=16, col=rainbow(4))
beeswarm(FSC.CV ~ fixative, data = data, pch=16, col=rainbow(4))
beeswarm(SSC.CV ~ fixative, data = data, pch=16, col=rainbow(4))

for (ii in c(1:length(sample_names))){
  data_fixative <- subset(data, sample==sample_names[ii])
  std <- sd(data_fixative$Cell.Count)
  mean <- mean(data_fixative$Cell.Count)
  delta <- abs(mean-mean*1.05)
  result <- power.t.test(d=delta, power=0.99, sd=std, n=NULL)
  total_samples <- round(result$n)
  repeat_times <- round(total_samples/5)

  data_fixative_simulated <- simulate_samples(data_fixative, 1.05*mean)
  data_fixative_simulated$time <- "20min"
  data_fixative <- simulate_samples(data_fixative, mean)

  data_fixative <- rbind(data_fixative, data_fixative_simulated)
  beeswarm(Cell.Count ~ time, data = data_fixative, main=paste(fixatives[ii], "Simulated 5% Increase, 99% Power"), pch=16, col=rainbow(4))
  legend("topleft",legend="", title=paste("Samples Needed: ", total_samples), box.lty=0)
}


dev.off()
