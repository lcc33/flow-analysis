


import_data <- function(){
  
  return(df)
}

filter_data <- function(df, excluded){
  df$Date <- as.Date(df$Date, format = "%m/%d/%y")
  for (exclude in excluded){
    df <- filter(df, Experiment != exclude)
  }
  df <- filter(df, Date > as.Date('3/3/21', format='%m/%d/%y'))
  overview <- generate_overview_2(df)
  #experiments_good <- filter(overview, mean_covariance<0.03)$Experiment
  #df <- filter(df, Experiment %in% experiments_good)
  return(df)
}

filter_data_2 <- function(df){
  overview <- generate_overview_2(df)

  experiments_good <- filter(overview, Water.Temperature>41.8, Water.Temperature<42.3)$Experiment
  df <- filter(df, Experiment %in% experiments_good)

  return(df)
}

normalize_cell_count <- function(df){
  df_baseline <- df %>%
    filter(Time==0) %>%
    group_by(Experiment) %>%
    summarise(baseline = mean(Cell.Count))# %>%
  df <- full_join(df, df_baseline) %>% 
    mutate(Cell.Count.Normalized = Cell.Count/baseline)
  return(df)
}


generate_overview_2 <- function(df){
  overview <- read.csv('aggregate - Experiments.csv', header=TRUE, colClasses=c('Date'='character'))
  experiments <- unlist(distinct(df, Experiment))
  overview <- filter(overview, Experiment %in% experiments)
  
  df_baseline <- df %>%
    filter(Time>30) %>%
    group_by(Experiment) %>%
    summarise(normalized_increase = mean(Cell.Count.Normalized))
  overview <- left_join(overview, df_baseline, by='Experiment')

  df_baseline <- df %>%
    filter(Time==0) %>%
    group_by(Experiment) %>%
    summarise(average_zero_count = mean(Cell.Count))
  overview <- left_join(overview, df_baseline, by='Experiment')

  df_group <- df %>%
    group_by(Experiment, Time) %>%
    summarise(mean_cell_count = mean(Cell.Count), 
      covariance = sd(Cell.Count)/mean(Cell.Count))
  df_group <- df_group %>%
    group_by(Experiment) %>%
    summarise(mean_covariance=mean(covariance))
  overview <- left_join(overview, df_group, by='Experiment')
  
  return(overview)
}

calculate_ideal_ratio <- function(points, dilution){
  ideal_ratio <- numeric(points)
  ideal_ratio[1] <- 100
  for (ii in c(2:(points))){  
    ideal_ratio[ii] <- ideal_ratio[ii-1]/dilution
  } 
  return(ideal_ratio)
}
