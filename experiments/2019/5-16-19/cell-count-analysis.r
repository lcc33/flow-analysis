library(matrixStats)
library(plotrix)
library(gplots)
library(xtable)
library(tibble)


read_csv_column <- function(file_name, column_number){
  data <- read.csv(file=file_name, header=TRUE, sep=',')
  column_data <- as.numeric(data[,column_number])
  return(column_data)
}


plot_error <- function(cells_total, color, time){
  sample_size <- dim(cells_total)[2]
  total_count_avg <- rowMeans(cells_total)
  total_count_stdev <- rowSds(cells_total)
  if (total_count_avg[1]==1){
    total_count_stdev[1] <- 0.01
  }
  total_count_error <- total_count_stdev*1.96/sqrt(sample_size)
  width = 1.5
  xleft <- time-width
  xright <- time+width
  ybottom <- total_count_avg-total_count_error
  ytop <- total_count_avg+total_count_error
  color = paste(color, "50", sep="")
  rect(xleft, ybottom, xright, ytop, col=color, border=NA, density=50)  
  par(new=TRUE)
}


normalize_t0 <- function(cells){
  for (ii in c(1:dim(cells)[2])){
    cells[,ii] = cells[,ii]/cells[1,ii]
  }
  return(cells)
}


determine_linear_range <- function(input, output){
  rows <- length(input)
  points <- 1

  r2 <- numeric(rows)
  slope <- numeric(rows)
  yint <- numeric(rows)
  for (ii in c((1+points):(rows-points))){
    y <- output[(ii-points):(ii+points)]
    x <- input[(ii-points):(ii+points)]
    results <- linear_fit(x, y)
    r2[ii] <- results$r2
    slope[ii] <- results$coeff[2]
    yint[ii] <- results$coeff[1]
  }
  best_fit <- which.max(r2)
  fit <- slope[best_fit]*input + yint[best_fit]
  print(slope[best_fit])
  ideal_count <- 10**(output[best_fit+points])
  results <- list("fit"=fit, "central_point"=best_fit+points, "ideal_count"=ideal_count, "r2"=r2, "slope"=slope)
  return(results)
}

linear_fit <- function(x, y){
  model <- lm(y ~ x)
  r2 <- summary(model)$r.squared
  coeff <- coefficients(model)
  results <- list("r2"=r2, "coeff"=coeff)
  return(results)
}

calculate_ideal_ratio <- function(points, dilution){
  ideal_ratio <- numeric(points)
  ideal_ratio[1] <- 100
  for (ii in c(2:(points))){  
    ideal_ratio[ii] <- ideal_ratio[ii-1]/dilution
  } 
  return(ideal_ratio)
}

dilution_finder <- function(file_name, dilution_factor, points, column, ylabel){
  dilution <- calculate_ideal_ratio(points, dilution_factor)

  cells <- read_csv_column(file_name, column) 
  cells <- cells[1:points]
  cells[cells<0.01] <- 1
  cells <- log10(cells)
  dilution <- log10(dilution)

  results <- determine_linear_range(dilution, cells)
  title <- paste("Ideal Count: ", results$ideal_count, " cells/min (low)")
  plot(dilution, cells, main=title, xlab="Dilution (log10)", ylab=ylabel)
  lines(dilution, results$fit)
  ideal_range_ind <- (results$r2>0.99) & (results$slope>0.95) & (results$slope<1.1)
  ideal_points <- length(ideal_range_ind)

  ideal_range <- results$fit[ideal_range_ind]
  print(ideal_range)
  points(dilution[ideal_range_ind], ideal_range, pch=1, cex=2, col="red")
  return(results$r2)
}


pdf("results_overall.pdf", 7, 10)
par(mfrow=c(2,1))

file_name <- "results.csv"
points <- 20
dilution_factor <- 2

column <- 3
ylabel <- "Single Cell Count (log10)"
r2 <- dilution_finder(file_name, dilution_factor, points, column, ylabel)

cells <- read_csv_column(file_name, 3)
doublets <- read_csv_column(file_name, 4)
doublet_prop <- doublets/(doublets + cells)
doublet_prop <- doublet_prop[1:20]*100
dilution <- log10(calculate_ideal_ratio(points, dilution_factor))
plot(dilution, doublet_prop, ylab="Doublet Proportion (per cent)", xlab="Dilution (log10)")

fold_change <- numeric(20)
for (ii in c(2:20)){
  fold_change[ii] <- cells[ii]/cells[ii-1]
}

table <- data.frame(
  cells=cells[1:20],
  fold_change = round(fold_change, 2),
  fit_analysis = round(r2, 4),
  doublets= doublets[1:20],
  doublet_prop=round(doublet_prop/100, 2)
)
textplot(table)
dev.off()



