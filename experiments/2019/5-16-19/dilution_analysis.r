library(dplyr)
library(tibble)
library(ggplot2)

library(modelr)
options(na.action = na.warn)


n_points <- 20
dilution_factor <- 2
#ideal_ratio <- calculate_ideal_ratio(n_points, dilution_factor)

model <- function(x_int, df) {
  x_int + df$Log.Concentration * -1
}


df <- read.csv('results.csv', header=TRUE)
df <- df %>% filter(File>0)
df <- df %>% mutate(
  Dilution.Factor = 2^(File-1),
  Concentration = 1/Dilution.Factor,
  Log.Concentration = log10(Concentration),
  Log.Total.Cells = log10(Cell.Count + Doublet.Count)
)



lower_inds = c(1:10)
upper_inds = c(11:20)
distance_from_actual <- 10
lower_ind_best <- 1
upper_ind_best <- 20
loss_best = 20

for (lower_ind in lower_inds){
  for (upper_ind in upper_inds){
    df_portion <- df %>% filter(
      File <= upper_ind,
      File >= lower_ind
    )
    fit_model <- lm(Log.Total.Cells ~ Log.Concentration, data=df_portion)
    loss <- abs(as.numeric(fit_model$coef[2]) - 1)
    if (loss < loss_best){
      lower_ind_best <- lower_ind
      upper_ind_best <- upper_ind
      loss_best <- loss
    }
  }
}

df_portion <- df %>% filter(
  File <= upper_ind_best,
  File >= lower_ind_best
)
fit_model <- lm(Log.Total.Cells ~ Log.Concentration, data=df_portion)

df <- df %>% mutate(
  Linear.Region = (File <= upper_ind_best) & (File >= lower_ind_best)
)

ggplot(data=df, aes(x=Log.Concentration)) + 
  geom_abline(aes(intercept = fit_model$coef[1], slope = fit_model$coef[2])) + 
  geom_point(mapping=aes(y=Log.Total.Cells, color=Linear.Region)) +
  xlab('Log10(Normalized Cell Concentration)') + 
  ylab('Log10(Cell Count)')
ggsave('figure.png', width=4, height=3)
print(df)



