from matplotlib import pyplot as plt
import numpy as np
import csv

def importData(fileName, column):
  data = []
  with open(fileName, 'r') as dataFile:
    dataImport = csv.reader(dataFile, delimiter=',')
    for row in dataImport:
      data.append(row[column])
  dataFile.close()
  data = data[1:]
  for ii in range(len(data)):
    data[ii] = float(data[ii])
  return data

file_name = 'conc_results.csv'
cellCount = importData(file_name, 2)
doubletCount = importData(file_name, 3)
totalCount = importData(file_name, 1)
averageSize = importData(file_name, 4)
averageSize = np.divide(averageSize, averageSize[0])

cellCount = np.divide(cellCount, totalCount)
doubletCount = np.divide(doubletCount, totalCount)

dilution = [10, 100, 1000, 10000]

ax = plt.axes()
ax.plot(dilution, cellCount)
ax.plot(dilution, doubletCount)
ax.plot(dilution, averageSize)
ax.set_title('Dilution Experiment')
ax.set_xscale('log')
ax.set_ylabel('Cell Count')
ax.set_xlabel('Dilution')
plt.legend(['Short Cell Proportion', 'Long Cell Proportion', 'Size'])

plt.show()