library(matrixStats)
library(plotrix)
library(xtable)

read_csv_column <- function(file_name, column_number){
  data <- read.csv(file=file_name, header=TRUE, sep=',')
  column_data <- as.numeric(data[,column_number])
  return(column_data)
}

plot_error <- function(cells_total, color, time){
  sample_size <- dim(cells_total)[2]
  total_count_avg <- rowMeans(cells_total)
  total_count_stdev <- rowSds(cells_total)
  if (total_count_avg[1]==1){
    total_count_stdev[1] <- 0.01
  }
  total_count_error <- total_count_stdev*1.96/sqrt(sample_size)
  width = 1.5
  xleft <- time-width
  xright <- time+width
  ybottom <- total_count_avg-total_count_error
  ytop <- total_count_avg+total_count_error
  color = paste(color, "50", sep="")
  rect(xleft, ybottom, xright, ytop, col=color, border=NA, density=50)  
  par(new=TRUE)
}

plot_cell_counts <- function(cells_total_30deg, cells_total_42deg, title, sample_names, ylabel, time){
  sample_size <- dim(cells_total_30deg)[2]  
  time_42deg <- time + 0.5
  color_30deg <- "#0577B1"
  color_42deg <- "#C84E00"
  max_value <- max(cells_total_30deg, cells_total_42deg) 
  shapes <- c(0, 1, 2, 3, 4, 5, 6, 7, 8)
  for (ii in c(1:sample_size)){    
    plot(time, cells_total_30deg[,ii], pch=shapes[ii], cex=1.5, col=color_30deg, xlab="Time (min)", ylab=ylabel, main=title, ylim=c(0, max_value))
    par(new=TRUE)
    points(time_42deg, cells_total_42deg[,ii], pch=shapes[ii], col=color_42deg, cex=1.5)
    par(new=TRUE)
  }
  plot_error(cells_total_30deg, color_30deg, time)
  plot_error(cells_total_42deg, color_42deg, time_42deg)
  legend("topleft", c("30 degrees", "42 degrees"), bty="n",pch=15, col=c(color_30deg, color_42deg))
  legend("bottomright", sample_names, bty="n",pch=shapes)
  par(new=FALSE)
}

extract_data <- function(file_names, folder, column, n){
  result <- matrix(nrow=n, ncol=length(file_names))
  for (ii in c(1:length(file_names))){
    file_name <- paste(folder, file_names[ii], sep="/")
    cells_short <- read_csv_column(file_name, 3)
    cells_long <- read_csv_column(file_name, 4)
    result[,ii] <- cells_long + cells_short
    result[,ii] <- result[,ii]/result[1,ii]
  }
  
  return(result)
}

pdf("results_time_course.pdf", 7, 10)
par(mfrow=c(2,1))

folder <- "results_JFL101_time_course"
title <- "JFL101, Short Cell Count, Medium Flow"
file_names <- c("results_30deg_med.csv", "results_30deg_high.csv", "results_42deg_med.csv", "results_42deg_high.csv")
ylabel <- "Short Cell Count"
sample_names <- c("Medium Flow", "High Flow")
time <- c(0, 15, 30, 60)
cells_total <- extract_data(file_names, folder, 3, 4)
print(cells_total)
plot_cell_counts(cells_total[,1:2], cells_total[,3:4], title, sample_names, ylabel, time)

dev.off()

