library(matrixStats)
library(plotrix)
library(gplots)
library(tibble)
library(xtable)

read_csv_column <- function(file_name, column_number){
  data <- read.csv(file=file_name, header=TRUE, sep=',')
  column_data <- as.numeric(data[,column_number])
  return(column_data)
}

plot_error <- function(input, output, color){
  sample_size <- dim(output)[2]
  total_count_avg <- rowMeans(output)
  total_count_stdev <- rowSds(output)
  if (total_count_avg[1]==1){
    total_count_stdev[1] <- 0.01
  }
  total_count_error <- total_count_stdev*1.96/sqrt(sample_size)
  width = 0.1
  xleft <- input-width
  xright <- input+width
  ybottom <- total_count_avg-total_count_error
  ytop <- total_count_avg+total_count_error
  color = paste(color, "50", sep="")
  rect(xleft, ybottom, xright, ytop, col=color, border=NA, density=50)  
  par(new=TRUE)
}

plot_cell_counts <- function(ideal_ratio, cells_flow){
  sample_size <- dim(cells_flow)[2]
  color_a <- "#0577B1"
  color_b <- "#C84E00"
  sample_names <- c("Flow Count", "Ideal Count")
  cells_flow <- log(cells_flow+0.01)
  ideal_ratio <- log(ideal_ratio+0.01)
  max_value <- max(cells_flow) 
  ylabel <- "Actual Cell Count (log)"
  for (ii in c(1:sample_size)){    
    plot(ideal_ratio, cells_flow[,ii], pch=20, cex=1, col=color_a, xlab="Dilution (log)", ylab=ylabel, main=title, ylim=c(0.1, max_value))
    par(new=TRUE)
  }
  plot_error(ideal_ratio, cells_flow, color_a)
  results <- determine_linear_range(ideal_ratio, cells_flow)
  points(ideal_ratio, results$fit, type="l")
  central_point <- results$central_point
  central_dilution <- ideal_ratio[central_point]
  central_cell_count <- mean(cells_flow[central_point,])
  points(central_dilution, central_cell_count, pch=1, cex=2, col=color_b)
  sample_names[2] <- paste(sample_names[2], round(results$ideal_count), "cells/min")
  legend("topleft", sample_names, bty="n",pch=c(20, 1), col=c(color_a, color_b))
  par(new=FALSE)
}

extract_data <- function(file_names, folder, column, n){
  result <- matrix(nrow=n, ncol=length(file_names))
  for (ii in c(1:length(file_names))){
    file_name <- paste(folder, file_names[ii], sep="/")
    result[,ii] <- read_csv_column(file_name, column)
  }
  return(result)
}

determine_linear_range <- function(input, output){
  rows <- dim(output)[1]
  cols <- dim(output)[2]
  r2 <- numeric(rows-2)
  slope <- numeric(rows-2)
  yint <- numeric(rows-2)
  points <- 3
  for (ii in c(1:(rows-2))){
    y <- as.vector(output[ii:(ii+2),])
    x <- input[ii:(ii+2)]
    x <- rep(x, times=cols)
    results <- linear_fit(x, y)
    r2[ii] <- results$r2
    slope[ii] <- results$coeff[2]
    yint[ii] <- results$coeff[1]
  }
  best_fit <- which.max(r2)
  fit <- slope[best_fit]*input + yint[best_fit]
  ideal_count <- exp(mean(output[best_fit+1,]))/2
  results <- list("fit"=fit, "central_point"=best_fit+1, "ideal_count"=ideal_count)
  return(results)
}

linear_fit <- function(x, y){
  model <- lm(y ~ x)
  r2 <- summary(model)$r.squared
  coeff <- coefficients(model)
  results <- list("r2"=r2, "coeff"=coeff)
  return(results)
}

calculate_ideal_ratio <- function(points, dilution){
  ideal_ratio <- numeric(points)
  ideal_ratio[1] <- 1
  for (ii in c(2:(points))){  
    ideal_ratio[ii] <- ideal_ratio[ii-1]/dilution
  } 
  return(ideal_ratio)
}

pdf("results_overall.pdf", 7, 10)
par(mfrow=c(2,1))

samples <- 6
table <- data.frame(
  id=c("a", "b", "c", "d", "e", "f"), 
  type=c("MC", "MC", "MC", "JFL", "JFL", "JFL"),
  flow=c("low", "med", "high","low", "med", "high"),
  dil_1 = numeric(samples),
  dil_2 = numeric(samples),
  dil_3 = numeric(samples)
)

folder <- "results_MC4100"

cells_total <- matrix(0L, nrow=6, ncol=3)

dilutions <- 7
file_names <- c("results_low_a.csv", "results_low_b.csv")
title <- "MC4100, Short Cell Count, Low Flow"
cells_flow <- extract_data(file_names, folder, 3, dilutions)
ideal_ratio <- calculate_ideal_ratio(dilutions, 4)
plot_cell_counts(ideal_ratio, cells_flow)

cells <- extract_data(file_names, folder, 3, dilutions) + extract_data(file_names, folder, 4, dilutions)
cells_total[1,] <- cells[1:3,1]

dilutions <- 5
title <- "MC4100, Short Cell Count, Medium Flow"
file_names <- c("results_med.csv", "results_med.csv")
cells_flow <- extract_data(file_names, folder, 3, dilutions)
ideal_ratio <- calculate_ideal_ratio(dilutions, 4)
plot_cell_counts(ideal_ratio, cells_flow)

cells <- extract_data(file_names, folder, 3, dilutions) + extract_data(file_names, folder, 4, dilutions)
cells_total[2,] <- cells[1:3,1]

title <- "MC4100, Short Cell Count, High Flow"
file_names <- c("results_high.csv", "results_high.csv")
cells_flow <- extract_data(file_names, folder, 3, dilutions)
ideal_ratio <- calculate_ideal_ratio(dilutions, 4)
plot_cell_counts(ideal_ratio, cells_flow)

cells <- extract_data(file_names, folder, 3, dilutions) + extract_data(file_names, folder, 4, dilutions)
cells_total[3,] <- cells[1:3,1]

plot.new()

folder <- "results_JFL101"
title <- "JFL101, Short Cell Count, Low Flow"
file_names <- c("results_low_a.csv", "results_low_b.csv")
cells_flow <- extract_data(file_names, folder, 3, dilutions)
ideal_ratio <- calculate_ideal_ratio(dilutions, 4)
plot_cell_counts(ideal_ratio, cells_flow)

title <- "JFL101, Long Cell Count, Low Flow"
cells_flow <- extract_data(file_names, folder, 4, dilutions)
ideal_ratio <- calculate_ideal_ratio(dilutions, 4)
plot_cell_counts(ideal_ratio, cells_flow)

cells <- extract_data(file_names, folder, 3, dilutions) + extract_data(file_names, folder, 4, dilutions)
cells_total[4,] <- cells[1:3,1]

dilutions <- 6
title <- "JFL101, Short Cell Count, Medium Flow"
file_names <- c("results_medium.csv", "results_medium.csv")
cells_flow <- extract_data(file_names, folder, 3, dilutions)
ideal_ratio <- calculate_ideal_ratio(dilutions, 4)
plot_cell_counts(ideal_ratio, cells_flow)


title <- "JFL101, Long Cell Count, Medium Flow"
cells_flow <- extract_data(file_names, folder, 4, dilutions)
ideal_ratio <- calculate_ideal_ratio(dilutions, 4)
plot_cell_counts(ideal_ratio, cells_flow)

cells <- extract_data(file_names, folder, 3, dilutions) + extract_data(file_names, folder, 4, dilutions)
cells_total[5,] <- cells[1:3,1]

title <- "JFL101, Short Cell Count, High Flow"
file_names <- c("results_high.csv", "results_high.csv")
cells_flow <- extract_data(file_names, folder, 3, dilutions)
ideal_ratio <- calculate_ideal_ratio(dilutions, 4)
plot_cell_counts(ideal_ratio, cells_flow)

title <- "JFL101, Long Cell Count, High Flow"
cells_flow <- extract_data(file_names, folder, 4, dilutions)
ideal_ratio <- calculate_ideal_ratio(dilutions, 4)
plot_cell_counts(ideal_ratio, cells_flow)

cells <- extract_data(file_names, folder, 3, dilutions) + extract_data(file_names, folder, 4, dilutions)
cells_total[6,] <- cells[1:3,1]

table[,4:6] <- cells_total

ratio_1 <-round(cells_total[,1]/cells_total[,2], digits=1)
ratio_2 <-round(cells_total[,2]/cells_total[,3], digits=1)

table_dil_change <- table
table_dil_change <- add_column(table_dil_change, "<-"=ratio_1, .after="dil_1")
table_dil_change <- add_column(table_dil_change, "<-."=ratio_2, .after="dil_2")

ratio <-round(cells_total[2,]/cells_total[3,], digits=1)

table_flow_change <- table
ratio <-round(cells_total[2,]/cells_total[1,], digits=1)
table_flow_change <- add_row(table_flow_change, dil_1=ratio[1], dil_2=ratio[2], dil_3=ratio[3], .after=1)

ratio <-round(cells_total[3,]/cells_total[2,], digits=1)
table_flow_change <- add_row(table_flow_change, dil_1=ratio[1], dil_2=ratio[2], dil_3=ratio[3], .after=3)

ratio <-round(cells_total[5,]/cells_total[4,], digits=1)
table_flow_change <- add_row(table_flow_change, dil_1=ratio[1], dil_2=ratio[2], dil_3=ratio[3], .after=6)


ratio <-round(cells_total[6,]/cells_total[5,], digits=1)
table_flow_change <- add_row(table_flow_change, dil_1=ratio[1], dil_2=ratio[2], dil_3=ratio[3], .after=8)

textplot(table_flow_change[1:5,3:6], valign="top")
title("MC4100: Flow Rate Fold Change")

textplot(table_flow_change[6:10,3:6], valign="top")
title("JFL101: Flow Rate Fold Change")

textplot(table_dil_change, valign="top")
title("Dilution Fold Change")


print(table_dil_change)
print(table_flow_change)

dev.off()

