import fcsparser as fcsp
from matplotlib import pyplot as plt
import numpy as np
from shapely.geometry import point as pt
from shapely.geometry import polygon as ply
from descartes import PolygonPatch
from os import listdir
from os.path import isfile, join


class flowAnalyzer():

  def __init__(self):
    self.reset()
    self.shiftClick = False

    self.fig = plt.figure(1)
  
    self.extractData()
    self.plotData()

  def extractData(self):
    path = '/home/lauren/Documents/Data/3-20-19/data/low/'
    self.files = [f for f in listdir(path) if isfile(join(path, f))]
    totalFiles = len(self.files)
    self.data = [None]*totalFiles
    self.cellCount = [0]*totalFiles
    self.polygonPatch = [None]*totalFiles
    for ii in range(totalFiles):
      meta, self.data[ii] = fcsp.parse(path+self.files[ii], meta_data_only=False, reformat_meta=True)
      
  def plotData(self):
    fig = plt.figure(1)
    fig.canvas.mpl_connect('button_press_event', self.onClick)
    fig.canvas.mpl_connect('key_press_event', self.onKey)
    totalFiles = len(self.files)
    totalPoints = len(self.xdata)
    for ii in range(totalFiles):
      if (self.axesClicked[ii]==True):
        axesColor = 'green'
      else:
        axesColor = 'white'
      self.ax[ii] = plt.subplot(2, 2, ii+1, facecolor=axesColor, alpha=0.5)
      plt.scatter(self.data[ii]['FSC-A'], self.data[ii]['SSC-A'], c='black', alpha=0.5)
      if (self.axesClicked[ii]==True):
        plt.scatter(self.xdata, self.ydata, c='red', alpha = 1)
        if (totalPoints>3):
          self.polygonPatch[ii].remove()
        if (totalPoints>2):
          self.createPolygon()
          self.polygonPatch[ii] = PolygonPatch(self.polygon, alpha=0.3)
          self.ax[ii].add_patch(self.polygonPatch[ii])
      plt.yscale('log')
      plt.ylim((10, 1000000))
      plt.xscale('log')
      plt.xlim((1000, 1000000))
      plt.title = self.files[ii]
    plt.show()
    self.shiftClick = False

  def reset(self):
    self.xdata = []
    self.ydata = []
    
    self.polygon = []
    self.ax = [None]*4
    self.axesClicked = [False]*4
    
  def startAnalysis(self):
    for jj in range(4):
      if (self.axesClicked[jj]==True):
        dataset = self.data[jj]
        for ii in range(dataset.shape[0]):
          point = pt.Point(dataset['FSC-A'][ii], dataset['SSC-A'][ii])
          if (self.polygon.contains(point)==True):
            self.cellCount[jj] += 1
        self.printOutput()

  def onClick(self, event):
    if (self.shiftClick==True):
      for ii in range(4):
        if (event.inaxes==self.ax[ii]):
          self.axesClicked[ii] = True
      self.shiftClick=False
    else: 
      print('hi')
      self.xdata.append(event.xdata)
      self.ydata.append(event.ydata)
        
    self.plotData()
    self.shiftClick = False

      
  def onKey(self, event):
    if (event.key=='enter'):
      self.startAnalysis()
    elif (event.key=='backspace'):
      self.reset()
    elif (event.key=='shift'):
      self.shiftClick = True

  def createPolygon(self):
    polygon = []
    for ii in range(len(self.xdata)):
      polygon.append((self.xdata[ii], self.ydata[ii]))
    self.polygon = ply.Polygon(polygon)
  
  def printOutput(self):
    fileId = open("results.csv", "w")
    line = 'File, Count\n' 
    fileId.write(line)
    for ii in range(4):
      if (self.axesClicked[ii]==True):
        line = self.files[ii]+ ', ' + str(self.cellCount[ii]) + '\n'
        print(line)
        fileId.write(line)
    fileId.close()
      
plot = flowAnalyzer()



