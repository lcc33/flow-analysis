import fcsparser as fcsp
from matplotlib import pyplot as plt
import numpy as np
from shapely.geometry import point as pt
from shapely.geometry import polygon as ply
from descartes import PolygonPatch

import os
from os import listdir
from os.path import isfile, join
from tkinter import filedialog
from tkinter import *
from matplotlib.widgets import Button

class flowAnalyzer():

  def __init__(self):
    self.extractData()
    self.reset()

    self.fig = plt.figure(1)
    self.fig.canvas.mpl_connect('button_press_event', self.onClick)
    self.fig.canvas.mpl_connect('key_press_event', self.onKey)
    self.plotAllData()
    
  def extractData(self):
    path = os.path.dirname(os.path.realpath(__file__))
    root = Tk()
    root.update()
    self.files =  filedialog.askopenfilenames(initialdir = path,title = "Select file",filetypes = (("fcs files","*"),("all files","*")))
    root.destroy()
    totalFiles = len(self.files)
    self.data = [None]*totalFiles
    self.cellCount = [0]*totalFiles
    self.boundary1 = []
    self.boundary2 = []
    for ii in range(totalFiles):
      meta, self.data[ii] = fcsp.parse(self.files[ii], meta_data_only=False, reformat_meta=True)
      rows = self.data[ii].shape[0]
      self.boundary1.append([True]*rows)
      self.boundary2.append([True]*rows)

  def plotAllData(self):
    self.plotData1()
    self.plotData2()
    self.plotData3() 
    plt.show()

  def plotData1(self):
    totalFiles = len(self.files)
    for ii in range(totalFiles):
      subplotIndex = 3*ii
      ax = self.fig.add_subplot(totalFiles, 3, subplotIndex+1)
      plt.cla()
      ax.set_title(self.files[ii][-7:-4])
      rows = self.data[ii].shape[0]
      colors = ['gray']*rows
      for jj in range(rows):
        if (self.boundary1[ii][jj]):
          colors[jj] = 'blue'
      ax.scatter(self.data[ii]['SSC-A'], self.data[ii]['FSC-A'], color=colors, alpha=0.1)
      #plt.xscale('log')

  def plotData2(self):
    totalFiles = len(self.files)
    for ii in range(totalFiles):
      subplotIndex = 3*ii
      ax = self.fig.add_subplot(totalFiles, 3, subplotIndex+2)
      plt.cla()
      rows = self.data[ii].shape[0]
      colors = ['gray']*rows
      for jj in range(rows):
        if (self.boundary1[ii][jj]):
          colors[jj] = 'blue'
        if (self.boundary2[ii][jj]):
          colors[jj] = 'green'
      ax.scatter(self.data[ii]['FSC-A'], self.data[ii]['FSC-H'], color=colors, alpha=0.1)

  def plotData3(self):
    totalFiles = len(self.files)
    for ii in range(totalFiles):
      subplotIndex = 3*ii
      ax = self.fig.add_subplot(totalFiles, 3, subplotIndex+3)
      plt.cla()
      rows = self.data[ii].shape[0]
      fsca = []
      for jj in range(rows):
        if (self.boundary1[ii][jj]) & (self.boundary2[ii][jj]):
          fsca.append(self.data[ii]['FSC-A'][jj])
      self.fscaMean = sum(fsca)/len(fsca)
      ax.hist(fsca, 50)
    plt.show()
  
  def showChosenArea(self):
    totalFiles = len(self.files)
    for ii in range(totalFiles):
      subplotIndex = ii*3
      ax = plt.subplot(totalFiles, 3, subplotIndex+1)
      ax.scatter(self.xbound1, self.ybound1, c='red', alpha = 1)
      ax = plt.subplot(totalFiles, 3, subplotIndex+2)
      ax.scatter(self.xbound2, self.ybound2, c='red', alpha = 1)
    plt.show()
   
  def showStandardBoundaries(self):
    self.xbound1 = [9170.269838937375, 40529.09127565286, 21886.096558914855, 12707.260660474836, 2674.145321120561, 1080.5819707158582, 901.4720368375014]
    self.ybound1 = [173592.7115221175, 157332.05414286954, 62269.749464189284, 8484.498132830602, -271.2404559951974, -776.0051104126032, 29509.234526651562]
    self.xbound2 = [25204.423081335903, 10804.904036438616, 191453.41569060407, 235961.0200111955]
    self.ybound2 = [0, 41114.5613175726, 220929.13409301406, 167107.08509900444]
    self.showChosenArea()

  def reset(self):
    self.xbound1 = []
    self.ybound1 = []
    self.xbound2 = []
    self.ybound2 = []
    self.polygon = []
    
  def applyBoundary1(self):
    self.createPolygon(self.xbound1, self.ybound1)
    totalFiles = len(self.files)
    for jj in range(totalFiles):
      dataset = self.data[jj]
      for ii in range(dataset.shape[0]):
        point = pt.Point(dataset['SSC-A'][ii], dataset['FSC-A'][ii])
        if (False == self.polygon.contains(point)):
          self.boundary1[jj][ii] = False

  def applyBoundary2(self):
    self.createPolygon(self.xbound2, self.ybound2)
    totalFiles = len(self.files)
    for jj in range(totalFiles):
      dataset = self.data[jj]
      for ii in range(dataset.shape[0]):
        point = pt.Point(dataset['FSC-A'][ii], dataset['FSC-H'][ii])
        if (False == self.polygon.contains(point)):
          self.boundary2[jj][ii] = False
    

  def onClick(self, event):
    self.xbound1.append(event.xdata)
    self.ybound1.append(event.ydata)
    self.showChosenArea()

  def onKey(self, event):
    if (event.key=='enter'):
      self.applyBoundary1()
      self.applyBoundary2()
      self.countCells()
      self.plotAllData()
      self.printOutput()
    elif (event.key=='backspace'):
      self.reset()
    elif (event.key=='a'):
      self.showStandardBoundaries()

  def countCells(self):
    for jj in range(len(self.files)):
      self.cellCount[jj] = 0
      for ii in range(self.data[jj].shape[0]):
        if (self.boundary2[jj][ii]) & (self.boundary1[jj][ii]):
          self.cellCount[jj] += 1

  def createPolygon(self, xbound, ybound):
    polygon = []
    for ii in range(len(xbound)):
      polygon.append((xbound[ii], ybound[ii]))
    totalPoints = len(xbound)
    if (totalPoints>2):
      self.polygon = ply.Polygon(polygon)
    
  def printOutput(self):
    totalFiles = len(self.files)
    fileId = open("results.csv", "w")
    line = 'File, Total Count, Boundary 1 Count\n' 
    fileId.write(line)
    for ii in range(totalFiles):
      line = self.files[ii][-7:-4]+ ', ' + str(self.data[ii].shape[0]) + ', ' + str(self.cellCount[ii]) + '\n'
      print(line)
      fileId.write(line)
    fileId.close()
      
plot = flowAnalyzer()



