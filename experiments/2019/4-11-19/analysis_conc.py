from matplotlib import pyplot as plt
import numpy as np
import csv

def importData(fileName, column):
  data = []
  with open(fileName, 'r') as dataFile:
    dataImport = csv.reader(dataFile, delimiter=',')
    for row in dataImport:
      data.append(row[column])
  dataFile.close()
  data = data[1:]
  for ii in range(len(data)):
    data[ii] = int(data[ii])
  return data

cellCount30 = importData('30deg_conc.csv', 2)
cellCount42 = importData('42deg_conc.csv', 2)
doubletCount30 = importData('30deg_conc.csv', 3)
doubletCount42 = importData('42deg_conc.csv', 3)

totalCells30 = np.add(cellCount30, doubletCount30)
totalCells42 = np.add(cellCount42, doubletCount42)

doubletProp30 = np.divide(doubletCount30, totalCells30)
doubletProp42 = np.divide(doubletCount42, totalCells42)


time = [10, 100, 1000, 10000]

ax = plt.subplot(2, 1, 1)
ax.plot(time, doubletProp42)
ax.set_title('30 Degrees')
ax.set_xscale('log')
ax.set_ylabel('Proportion of Doublets')
ax.set_xlabel('Dilution')

ax = plt.subplot(2, 1, 2)
ax.plot(time, doubletProp42)
ax.set_xscale('log')
ax.set_title('42 Degrees')
ax.set_xlabel('Dilution')
ax.set_ylabel('Proportion of Doublets')
plt.show()