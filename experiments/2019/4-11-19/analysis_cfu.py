from matplotlib import pyplot as plt
import numpy as np
import csv

def importData(fileName, column):
  data = []
  with open(fileName, 'r') as dataFile:
    dataImport = csv.reader(dataFile, delimiter=',')
    for row in dataImport:
      data.append(row[column])
  dataFile.close()
  data = data[1:]
  for ii in range(len(data)):
    data[ii] = int(data[ii])
  return data

time = importData('cfuCount.csv', 0)
cellCount30 = importData('cfuCount.csv', 1)
cellCount42 = importData('cfuCount.csv', 2)
plt.plot(time, cellCount30)
plt.plot(time, cellCount42)
plt.legend(['30 degrees', '42 degrees'])
plt.title('CFU Count over Time')
plt.show()