library(matrixStats)
library(plotrix)
library(ggplot2)
library(xtable)

read_csv_column <- function(file_name, column_number){
  data <- read.csv(file=file_name, header=TRUE, sep=',')
  column_data <- as.numeric(data[,column_number])
  return(column_data)
}

read_flow_data <- function(column, samples, time){
  n <- length(samples)  
  cells <- matrix(nrow=length(time), ncol=n)
  for (ii in c(1:n)){
    file_name <- paste("results_", samples[ii],".csv",sep='')
    cells[,ii] <- read_csv_column(file_name, column)
  }
  return(cells)
}

plot_error <- function(cells_total, color, time){
  sample_size <- dim(cells_total)[2]
  total_count_avg <- rowMeans(cells_total)
  total_count_stdev <- rowSds(cells_total)
  if (total_count_avg[1]==1){
    total_count_stdev[1] <- 0.01
  }
  total_count_error <- total_count_stdev*1.96/sqrt(sample_size)
  width = 1.5
  xleft <- time-width
  xright <- time+width
  ybottom <- total_count_avg-total_count_error
  ytop <- total_count_avg+total_count_error
  color = paste(color, "50", sep="")
  rect(xleft, ybottom, xright, ytop, col=color, border=NA, density=50)  
  par(new=TRUE)
}

plot_cell_counts <- function(cells_total_30deg, cells_total_42deg, title, sample_names, ylabel, time){
  sample_size <- dim(cells_total_30deg)[2]
  color_30deg <- "#0577B1"
  color_42deg <- "#C84E00"
  max_value <- max(cells_total_30deg, cells_total_42deg) * 1.1
  min_value <- min(cells_total_30deg, cells_total_42deg) * 0.9
  shapes <- c(0, 1, 2, 3, 4, 5, 6, 7, 8)
  plot(time, cells_total_30deg, pch=15, cex=1, col=color_30deg, xlab="Time (min)", ylab=ylabel, main=title, ylim=c(min_value, max_value), xlim=c(0, time[length(time)]+5))
  par(new=TRUE)
  points(time+0.5, cells_total_42deg, pch=15, col=color_42deg, cex=1)
  par(new=TRUE)
  for (ii in c(1:length(time))){
    label = signif(cells_total_30deg[ii], digits=4)
    text(time[ii], cells_total_30deg[ii], label, pos=4)
    if (abs(cells_total_30deg[ii]-cells_total_42deg[ii])>max_value*0.02){
      label = signif(cells_total_42deg[ii], digits=4)
      text(time[ii]+0.5, cells_total_42deg[ii], label, pos=4)
    }
  }
  legend("topleft", c("30 degrees", "42 degrees"), bty="n",pch=15, col=c(color_30deg, color_42deg))
  par(new=FALSE)

}


normalize_t0 <- function(cells){
  for (ii in c(1:dim(cells)[2])){
    cells[,ii] = cells[,ii]/cells[1,ii]
  }
  return(cells)
}

pdf("results_overall.pdf", 7, 10)
par(mfrow=c(2,1))
time <- c(0, 10, 20, 30)
sample_names <- c("30deg", "42deg")
title <- "May 29, 2019 Flow"

single_cells <- read_flow_data(3, sample_names, time)
ylabel <- "Single Cell Count"
plot_cell_counts(single_cells[,1], single_cells[,2], title, sample_names, ylabel, time)


double_cells <- read_flow_data(4, sample_names, time)
ylabel <- "Doublet Cell Count"
plot_cell_counts(double_cells[,1], double_cells[,2], title, sample_names, ylabel, time)

cells = single_cells+double_cells


percent_doublet <- double_cells/cells
ylabel <- "Percent Doublet"
plot_cell_counts(percent_doublet[,1], percent_doublet[,2], title, sample_names, ylabel, time)

ylabel <- "Total Cell Count"
plot_cell_counts(cells[,1], cells[,2], title, sample_names, ylabel, time)

cells <- normalize_t0(cells)
ylabel <- "Normalized Cell Count"
plot_cell_counts(cells[,1], cells[,2], title, sample_names, ylabel, time)

dev.off()



