library(matrixStats)

extract_total_cells_cfu <- function(cols){
  data <- read.csv(file="data.csv", header=FALSE, sep=',')
  total_cells = as.matrix(data[,cols])
}

total_cells_30 <- extract_total_cells_cfu(1:2)
total_cells_42 <- extract_total_cells_cfu(3:4)
time <- c(0, 15, 30, 60, 90)
plot(time, total_cells_30[,1])