from matplotlib import pyplot as plt
import numpy as np
import csv

def importData(fileName, column, start):
  data = []
  with open(fileName, 'r') as dataFile:
    dataImport = csv.reader(dataFile, delimiter=',')
    for row in dataImport:
      data.append(row[column])
  dataFile.close()
  data = data[start:]
  for ii in range(len(data)):
    data[ii] = int(data[ii])
  return data

def experimentalData(fileName, ax):
  time = [0, 15, 30, 60, 90]
  cellCount = importData(fileName, 2, 1)
  
  doubletCount = importData(file_name, 3, 1)
  totalCells = cellCount[0] + doubletCount[0]
  cellCount = np.divide(cellCount, totalCells)
  doubletCount = np.divide(doubletCount, totalCells)
  ax.scatter(time, cellCount[:5])
  ax.scatter(time, doubletCount[:5])
  return cellCount, doubletCount

def runModel(k_septate, cell, cell_septated, cell_elongated, ax, plot_type):
  proportion_septated = cell_septated[0]/(cell[0]+cell_septated[0])
  k_divide = float(1/(45*proportion_septated))
  
  k_elongate = float(1/75)
  k_death = float(1/50)  

  dt = 0.1
  time_total = 90
  time = [0]

  while (time[-1] < time_total):
    cell_septated_change = -k_divide*cell_septated[-1] + k_septate*cell[-1]
    cell_change = 2*k_divide*cell_septated[-1] - k_septate*cell[-1] - k_elongate*cell[-1]
    cell_elongated_change = k_elongate*cell[-1] - k_death*cell_elongated[-1]

    cell.append(cell[-1]+cell_change*dt)
    cell_septated.append(cell_septated[-1]+cell_septated_change*dt)
    cell_elongated.append(cell_elongated[-1]+cell_elongated_change*dt)
    time.append(time[-1]+dt)
  
  cell_singlet_total = np.add(cell, cell_septated)
  cell_total = cell_singlet_total[0] + cell_elongated[0]
  cell_singlet_total = np.divide(cell_singlet_total, cell_total)
  cell_elongated = np.divide(cell_elongated, cell_total)

  if (plot_type==1):
    ax.plot(time, cell_singlet_total)
    ax.plot(time, cell_elongated)
    ax.legend(['Normal Cells', 'Elongated Cells'])
  else:
    ax.plot(time, np.add(cell_singlet_total, cell_elongated))
    ax.legend(['Total CFUs'])

def testParameters(file_name, k_septate, proportion_septated, ax):
  cellCount, doubletCount = experimentalData(file_name, ax)
  cell = [cellCount[0]*(1-proportion_septated)]
  cell_septated = [cellCount[0]*proportion_septated]
  cell_elongated = [doubletCount[0]]

  runModel(k_septate, cell, cell_septated, cell_elongated, ax, 1)

proportion_septated = 0.4
rows = 2
cols = 2

plt.figure()
print(proportion_septated)
ax = plt.subplot(rows, cols, 1)
k_septate = float(1/(45*(1-proportion_septated)))
file_name = '3-29-19/JFL101_30degrees.csv'
testParameters(file_name, k_septate, proportion_septated, ax)

ax = plt.subplot(rows, cols, 2)
k_septate = 0
file_name = '3-29-19/JFL101_42degrees.csv'
testParameters(file_name, k_septate, proportion_septated, ax)

ax = plt.subplot(rows, cols, 3)
file_name = '4-5-19/30deg.csv'
k_septate = float(1/(45*(1-proportion_septated)))
testParameters(file_name, k_septate, proportion_septated, ax)

ax = plt.subplot(rows, cols, 4)
file_name = '4-5-19/42deg.csv'
k_septate = 0
testParameters(file_name, k_septate, proportion_septated, ax)

ax = plt.subplot(rows, cols, 1)
ax.set_title('30 Degrees')
ax = plt.subplot(rows, cols, 2)
ax.set_title('42 Degrees')

plt.show()



