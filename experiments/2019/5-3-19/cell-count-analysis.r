library(matrixStats)
library(plotrix)
library(ggplot2)

read_csv_column <- function(file_name, column_number){
  data <- read.csv(file=file_name, header=TRUE, sep=',')
  column_data <- as.numeric(data[,column_number])
  return(column_data)
}

read_flow_data <- function(column, samples, time){
  n <- length(samples)  
  cells <- matrix(nrow=length(time), ncol=n*2)
  for (ii in c(1:n)){
    file_name <- paste("results_30deg", samples[ii],"_original.csv",sep='')
    if (!file.exists(file_name)){
      file_name <- paste("results_30deg", samples[ii],".csv",sep='')
    }
    cells[,ii] <- read_csv_column(file_name, column)
    file_name <- paste("results_42deg", samples[ii],"_original.csv",sep='')
    if (!file.exists(file_name)){
      file_name <- paste("results_42deg", samples[ii],".csv",sep='')
    }
    cells[,ii+n] <- read_csv_column(file_name, column)
  }
  return(cells)
}

read_cfu_data <- function(folder, time){
  file_name <- paste("results_cfu.csv", sep='')
  cells_total <- matrix(nrow=length(time), ncol=4)
  for (ii in c(1:4)){
    cells_total[,ii] <- read_csv_column(file_name, ii+1)
  }
  return(cells_total)
}

plot_error <- function(cells_total, color, time){
  sample_size <- dim(cells_total)[2]
  total_count_avg <- rowMeans(cells_total)
  total_count_stdev <- rowSds(cells_total)
  if (total_count_avg[1]==1){
    total_count_stdev[1] <- 0.01
  }
  total_count_error <- total_count_stdev*1.96/sqrt(sample_size)
  width = 1.5
  xleft <- time-width
  xright <- time+width
  ybottom <- total_count_avg-total_count_error
  ytop <- total_count_avg+total_count_error
  color = paste(color, "50", sep="")
  rect(xleft, ybottom, xright, ytop, col=color, border=NA, density=50)  
  par(new=TRUE)
}

plot_cell_counts <- function(cells_total_30deg, cells_total_42deg, title, sample_names, ylabel, time){
  sample_size <- dim(cells_total_30deg)[2]
  time_42deg <- time + 0.5
  color_30deg <- "#0577B1"
  color_42deg <- "#C84E00"
  max_value <- max(cells_total_30deg, cells_total_42deg) 
  shapes <- c(0, 1, 2, 3, 4, 5, 6, 7, 8)
  for (ii in c(1:sample_size)){    
    plot(time, cells_total_30deg[,ii], pch=shapes[ii], cex=1.5, col=color_30deg, xlab="Time (min)", ylab=ylabel, main=title, ylim=c(0, max_value))
    par(new=TRUE)
    points(time_42deg, cells_total_42deg[,ii], pch=shapes[ii], col=color_42deg, cex=1.5)
    par(new=TRUE)
  }
  plot_error(cells_total_30deg, color_30deg, time)
  plot_error(cells_total_42deg, color_42deg, time_42deg)
  legend("topleft", c("30 degrees", "42 degrees"), bty="n",pch=15, col=c(color_30deg, color_42deg))
  legend("bottomright", sample_names, bty="n",pch=shapes)
  par(new=FALSE)
}
pdf("results_overall.pdf", 7, 10)
par(mfrow=c(2,1))
time <- c(0, 15, 30, 60)
sample_names <- c("a", "b", "c")
cells <- read_flow_data(3, sample_names, time)
print(cells)
cells_30deg <- cells[,1:4]
cells_42deg <- cells[,5:8]
title <- "May 3, 2019 Flow"
ylabel <- "Short Cell Count"
plot_cell_counts(cells_30deg, cells_42deg, title, sample_names, ylabel, time)
dev.off()

