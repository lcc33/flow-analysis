library(wesanderson)
df <- read.csv('results.csv')
pal <- wes_palette("BottleRocket2")
palette(pal)

plot(Singlets~Time, data=df, col=Temperature, pch=16)