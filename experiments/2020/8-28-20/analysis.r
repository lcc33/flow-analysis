source("/home/lauren/Documents/research/cell-counting/analysis-lib.r")
require(dplyr)

df <- read.csv('results.csv')
colorPalette <- generateColorPalette(1)
palette(colorPalette)
temperatures <- unique(df$Temperature)
rowCount <- nrow(df)
count <- 1
shapes <- integer(rowCount)
for (temperature in temperatures){
  ind <- which(temperature==df$Temperature)
  shapes[ind] <- count + 15
  count <- count + 1
}
df['Total'] <- df$Singlets + df$Doublets
plot(Total~Time, data=df, col=Temperature, pch=shapes, ylim=c(55000, 85000))


time <- unique(df$Time)

initialCount <- mean(filter(df, Time==0 & Temperature==42)$Cells)
finalCountTempShift <- mean(filter(df, Time==max(time) & Temperature==42)$Cells)
increase <- (finalCountTempShift - initialCount)/initialCount
print(paste0('Increase 42 degrees: ', increase))

initialCount <- mean(filter(df, Time==0 & Temperature==30)$Cells)
finalCountTempShift <- mean(filter(df, Time==max(time) & Temperature==30)$Cells)
increase <- (finalCountTempShift - initialCount)/initialCount
print(paste0('Increase 30 degrees: ', increase))

kStart <- log(finalCountTempShift/initialCount)*60/max(time)
dfSlice <- filter(df, Temperature==30)
model <- nls(Cells ~ y0 * exp(k/60 * Time), data=dfSlice, start=list(k=kStart, y0=initialCount))
print(model)
fit <- predict(model, list(x=dfSlice$Time))
lines(dfSlice$Time, fit)