source("/home/lauren/Documents/research/cell-counting/analysis-lib.r")
require(dplyr)
library(RColorBrewer)
require(nls)

df <- read.csv('results.csv')
df['Total'] <- df$Cells + df$Doublets

colorPalette <- generateColorPalette(1)
palette(colorPalette)

df$Temperature <- as.factor(df$Temperature)
dfTemperature <- with(df, data.frame(Temperature = levels(Temperature), color = colorPalette[1:nlevels(Temperature)], shape=c(16:(15+nlevels(Temperature)))))
df <- merge(df, dfTemperature)

plot(Cells~Time, data=df, col=color, pch=shape)
legend('topleft', legend=dfTemperature$Temperature, pch=dfTemperature$shape, col=dfTemperature$color, title='Temperature')
title('Cell Count over Time (min)')

time <- unique(df$Time)

initialCount <- mean(filter(df, Time==0 & Temperature==42)$Cells)
finalCountTempShift <- mean(filter(df, Time==max(time) & Temperature==42)$Cells)
increase <- (finalCountTempShift - initialCount)/initialCount
print(paste0('Increase 42 degrees: ', increase))

initialCount <- mean(filter(df, Time==0 & Temperature==30)$Cells)
finalCountTempShift <- mean(filter(df, Time==max(time) & Temperature==30)$Cells)
increase <- (finalCountTempShift - initialCount)/initialCount
print(paste0('Increase 30 degrees: ', increase))

kStart <- log(finalCountTempShift/initialCount)*60/max(time)
dfSlice <- filter(df, Temperature==30)
model <- nls(Cells ~ y0 * exp(k/60 * Time), data=dfSlice, start=list(k=kStart, y0=initialCount))
print(model)
fit <- predict(model, list(x=dfSlice$Time))
lines(dfSlice$Time, fit)