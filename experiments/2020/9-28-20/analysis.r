source("/home/lauren/Documents/research/cell-counting/analysis-lib.r")
require(dplyr)
library(RColorBrewer)

df <- read.csv('results.csv')

colorPalette <- generateColorPalette(1)
palette(colorPalette)

plot(Count~Treatment, data=df)
points(Count~Treatment, data=df)

result <- aov(Count~Treatment, data=df)
TukeyHSD(result)

ag <- aggregate(Count ~ Treatment, data=df, function(x) c(mean=mean(x), sd=sd(x)))
(ag)

title('Cell Count for Sources of Error')
df <- filter(df, Treatment=='A')
plot(Count~Sample, data=df)
title('Repeated Measures over 90 minutes')