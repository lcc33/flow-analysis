from FlowCytometryTools import FCMeasurement, PolyGate, FCPlate, ThresholdGate, QuadGate
from matplotlib.backends.backend_pdf import PdfPages
import wx
from pylab import *
import numpy as np
import os


def printCount(countName, counts):
  print('-----------')
  print(countName)
  for count in counts:
    print(count)

fileNameStart = '/home/lauren/Documents/research/cell-counting/experiments/4-6-21/data/'

#PFA Cell Gate:
#cellGate = PolyGate([(1.724e+03, 2.310e+03), (4.317e+03, 1.795e+03), (7.314e+03, 4.371e+03), (9.936e+03, 8.697e+03), (8.662e+03, 9.702e+03), (2.683e+03, 7.216e+03), (-7.423e+01, 4.551e+03)], ('FSC-A', 'SSC-A'), region='in', name='cell')
#doubletGate = ~ PolyGate([(1.574e+03, 7.710e+02), (9.806e+03, 9.393e+03), (9.375e+03, 9.898e+03), (-5.387e+02, 8.341e+02)], ('FSC-A', 'FSC-H'), region='in', name='doublet') & cellGate

cellGate = PolyGate([(4.809e+03, 2.645e+03), (6.995e+03, 4.525e+03), (9.927e+03, 8.659e+03), (8.854e+03, 9.715e+03), (4.531e+03, 6.920e+03), (1.972e+03, 5.079e+03), (3.132e+03, 3.006e+03)], ('FSC-A', 'SSC-A'),region='in', name='cell')




pdf = matplotlib.backends.backend_pdf.PdfPages('output.pdf')
fig = plt.figure(figsize=(8.5, 11))
fig.tight_layout()
fileNames = os.listdir(fileNameStart)
fileNames = sorted(fileNames)

nFiles = len(fileNames)
cellCount = np.zeros(nFiles)
doubletCount = np.zeros(nFiles)
cellWidth = np.zeros(nFiles)
cellHeight = np.zeros(nFiles)

ii = 0
for fileName in fileNames:

  print(fileName)
  sample = FCMeasurement(ID='Test Sample', datafile=(fileNameStart + fileName))
  tsample = sample.transform('hlog', channels=['SSC-W', 'SSC-A', 'SSC-H','FSC-A', 'FSC-H', 'FSC-W'], b=500.0) #, 'Alexa Fluor 488-A', 'Alexa Fluor 488-H', 'Alexa Fluor 488-W'
 # if ii+1 == 1:
 #  tsample.view_interactively(backend='wx')
  
  cells = tsample.gate(cellGate)
  cellCount[ii] = cells.shape[0]#tsample.shape[0]# 
  cellWidth[ii] = np.mean(cells['FSC-W'])
  cellHeight[ii] = np.mean(cells['FSC-H'])

  doublets = tsample.gate(doubletGate)
  doubletCount[ii] = doublets.shape[0]

  if True: 
    ax = plt.subplot(3, 2, (ii%6)+1)
    tsample.plot(['FSC-A', 'SSC-A'], gates=[cellGate])
    plt.title('Sample ' + str(ii+1))
    ax.xaxis.label.set_size(8)
    ax.yaxis.label.set_size(8)

    if (ii+1)%6==0 or ii==nFiles-1:
      pdf.savefig(fig)
      fig.clear()
  ii += 1
pdf.close()

printCount('Cell Count', cellCount)
printCount('Doublet Count', doubletCount)
printCount('Cell Width', cellWidth)
printCount('Cell Height', cellHeight)
