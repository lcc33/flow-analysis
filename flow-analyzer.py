import fcsparser as fcsp
from matplotlib import pyplot as plt
import numpy as np
from shapely.geometry import point as pt
from shapely.geometry import polygon as ply
import shapely.speedups
import pandas as pd

import os
from os import listdir
from os.path import isfile, join
from tkinter import filedialog
from tkinter import *
from matplotlib.widgets import Button
import time


import seaborn as sns
sys.path.append('FlowGrid')
from FlowGrid import FlowGrid
import scanpy as sc
import scanpy.external as sce

# profiler use: 'python -m cprofilev flow-analyzer.py'

class flowAnalyzer():
  def __init__(self):
    self.extractData()
    #self.combinationPlot()
    self.fig = plt.figure(1)
    self.fig.canvas.mpl_connect('button_press_event', self.onClick)
    sc.tl.pca(self.data, n_comps=5)
    self.clusterData()
    #self.applyStandardBoundaries()
    self.countCells()
    self.printOutput()
    self.plotAllData()

  def combinationPlot(self):
    sns.pairplot(self.data.iloc[:10000,:], plot_kws={"color":"darkred","alpha":0.01,"s":5})
    plt.show()

  def plotAllData(self):
    self.plot2dHist('SSC-W', 'Alexa Fluor 488-W', 'log',  1)
    self.plot2dHist('FSC-H', 'Alexa Fluor 488-H', 'log', 2)
    plt.show()

  def applyStandardBoundaries(self):
    #self.applyBoundary('bounds_cells_1.csv', 'FSC-H', 'SSC-H', 1)
    #self.applyBoundary('bounds_cells_4.csv', 'FSC-W', 'Alexa Fluor 488-W', 1)
    #self.applyBoundary('bounds/bounds_cells_3.csv','FSC-W', 'SSC-H', 1)
    #self.addBoundary('bounds/bounds_cells_2.csv', 'SSC-W', 'Alexa Fluor 488-W', 1, 1)
    #self.shapeBoundary(2)
    self.applyBoundary('bounds/bounds_beads.csv', 'FSC-A', 'Alexa Fluor 488-A', 2)
    
  def countCells(self):
    totalFiles = len(self.files)
    beadCol = self.result.columns.get_loc('beadCount')
    cellCol = self.result.columns.get_loc('cellCount')
    uncatCol = self.result.columns.get_loc('uncatCount')
    for jj in range(totalFiles):
      data = self.data[self.data['fileInd']==jj]
      self.result.iloc[jj, uncatCol] = data[data['type']==0].shape[0]
      self.result.iloc[jj, cellCol] =  data[data['type']==2].shape[0]# + 2*data[data['type']==2].shape[0]
      self.result.iloc[jj, beadCol] = data[data['type']==1].shape[0]
    self.result.cellCountNormalized = (self.result.cellCount)/self.result.beadCount*1000

  def extractData(self):
    path = os.path.dirname(os.path.realpath(__file__))
    root = Tk()
    root.update()
    self.files =  filedialog.askopenfilenames(initialdir = path,title = "Select file",filetypes = (("fcs files","*"),("all files","*")))
    root.destroy()
    #self.files = ['/home/lauren/Documents/research/cell-counting/1-21-20/data/Specimen_001_Tube_004_006.fcs']
    totalFiles = len(self.files)
    self.data = [None]*totalFiles
    self.result = pd.DataFrame(index=range(totalFiles), columns = ['fileName', 'fileInd', 'cellCount', 'cellCountNormalized', 'beadCount'])

    meta, self.data = fcsp.parse(self.files[0], meta_data_only=False, reformat_meta=True) 
    self.data['fileInd'] = int(0)
    fileName = [os.path.basename(self.files[0])]
    for ii in range(1, totalFiles):
      meta, data = fcsp.parse(self.files[ii], meta_data_only=False, reformat_meta=True) 
      data['fileInd'] = int(ii)
      self.data = self.data.append(data)
      fileName.append(os.path.basename(self.files[ii]))
    self.result.loc[:, 'fileName'] = fileName
    self.result.loc[:, 'fileInd'] = range(totalFiles)
    self.data['type'] = 0
    self.result['uncatCount'] = 0

  
  
  def plot2dHist(self, xdata, ydata, xscale, offset):
    totalFiles = len(self.files)
    for ii in range(totalFiles):
      ax = self.fig.add_subplot(totalFiles, 2, 2*ii+offset)
      data = self.data[self.data.fileInd==ii]
      plt.cla()
      ax.set_title(os.path.basename(self.files[ii]))
      colorColumn = data.type.to_numpy()
      x = data[xdata].to_numpy()
      y = data[ydata].to_numpy()
      cmap = sns.cubehelix_palette(dark=.8, light=.3, as_cmap=True)
      sns.scatterplot(x, y, hue=colorColumn , palette="Set2", alpha=0.01)#,s=1,  alpha=0.01)#, cmap='prism')
      ax.set_ylim([1, 300000])
      ax.set_xlim([1+10, 300000])
      ax.set_xscale(xscale)
      ax.set_yscale(xscale)
      ax.set_ylabel(ydata)
      ax.set_yticklabels([])
      ax.set_xticklabels([])
    ax.legend()
   
  

  def addBoundary(self, fileName, xcomponent, ycomponent, boundaryNameNew, boundaryNameOld):
    rows = self.data.shape[0]
    poly = self.createPolygon(fileName)
    colInd1 = self.data.columns.get_loc(xcomponent)
    colInd2 = self.data.columns.get_loc(ycomponent)
    xdata = self.data.iloc[:, colInd1].to_numpy()
    ydata = self.data.iloc[:, colInd2].to_numpy()
    result = self.data.loc[:, 'type'].to_numpy()
    for ii in range(rows):
      resultInd = self.determineBoundary(pt.Point(xdata[ii], ydata[ii]), poly) 
      if (resultInd) and (result[ii]==boundaryNameOld):
        result[ii] = boundaryNameNew
      else:
        result[ii] = 0
    self.data.loc[:, 'type'] = result

  def applyBoundary(self, fileName, xcomponent, ycomponent, boundaryName):
    rows = self.data.shape[0]
    poly = self.createPolygon(fileName)
    colInd1 = self.data.columns.get_loc(xcomponent)
    colInd2 = self.data.columns.get_loc(ycomponent)
    xdata = self.data.iloc[:, colInd1].to_numpy()
    ydata = self.data.iloc[:, colInd2].to_numpy()
    startTime = time.time()
    result = self.data.loc[:, 'type'].to_numpy()
    for ii in range(rows):
      resultInd = self.determineBoundary(pt.Point(xdata[ii], ydata[ii]), poly) 
      if (resultInd):
        result[ii] = boundaryName
    endTime = time.time()
    self.data.loc[:, 'type'] = result
    print(endTime-startTime)

  def shapeBoundary(self, boundaryName):
    rows = self.data.shape[0]
    
    colInd1 = self.data.columns.get_loc('Alexa Fluor 488-A')
    colInd2 = self.data.columns.get_loc('Alexa Fluor 488-H')
    colInd3 = self.data.columns.get_loc('type')

    xdata = self.data.iloc[:, colInd1].to_numpy()
    ydata = self.data.iloc[:, colInd2].to_numpy()
    currentId = self.data.iloc[:, colInd3].to_numpy()
    startTime = time.time()
    result = self.data.loc[:, 'type'].to_numpy()
    for ii in range(rows):
      resultInd = (xdata[ii]/ydata[ii] > 1.5)
      if resultInd and (currentId[ii]==1 or currentId[ii]==1):
        result[ii] = boundaryName
    endTime = time.time()
    self.data.loc[:, 'type'] = result
    print(endTime-startTime)
     
    

  def determineBoundary(self, point, poly):
    if (poly.contains(point)):
      return True
    else:
      return False

  def onClick(self, event):
    print(event.xdata, event.ydata)

  def clusterData(self):
    bin_n = 1
    eps = 1.3
    fileIndCol = self.data.columns.get_loc('fileInd')
    typeCol = self.data.columns.get_loc('type')

   # for ii in range(len(self.files)):
   #   data = self.data[self.data['fileInd']==ii]
   #   fg = FlowGrid(data, 5)#, bin_n=bin_n, eps=eps)
   #   label=fg.clustering()
   #   ind = self.data.index[self.data['fileInd']==ii].tolist()
   #   self.data.iloc[ind, typeCol] = label
    fg = FlowGrid(self.data, bin_n=bin_n, eps=eps)
    label = fg.clustering()
    self.data.iloc[:, typeCol] = label
    print(label)

 

  def createPolygon(self, fileName):
    data = pd.read_csv(fileName, header=None)
    xbound = data.iloc[0,:]
    ybound = data.iloc[1,:]
    polygon = []
    for ii in range(len(xbound)):
      polygon.append((xbound[ii], ybound[ii]))
    totalPoints = len(xbound)
    if (totalPoints>2):
      poly = ply.Polygon(polygon)
    return poly
    
  def printOutput(self):
    print(self.result)
    self.result.to_csv('result.csv', sep=",", float_format='%.2f',index=False, line_terminator='\n',encoding='utf-8')
    if (len(self.files)>2):

      print(np.divide(np.divide(self.result.cellCount.iloc[:-1], self.result.cellCount.iloc[1:]), 2))
      print(np.divide(np.divide(self.result.cellCountNormalized.iloc[:-1], self.result.cellCountNormalized.iloc[1:]), 2))
      
plot = flowAnalyzer()



