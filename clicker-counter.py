from tkinter import *
import matplotlib.pyplot as plt

class clickerCounter():

  def __init__(self):
    self.fig = plt.figure(1)
    self.cellCount = [0]*10
    self.activeCell = 0
    self.fig.canvas.mpl_connect('key_press_event', self.onKey)
    self.ax = plt.axes()
    plt.show()

  def onKey(self, event):
    if (event.key=='up'):
      self.cellCount[self.activeCell] += 1
    elif (event.key=='down'):
      self.cellCount[self.activeCell] += -1
    elif (event.key=='left'):
      self.activeCell += -1
    elif (event.key=='right'):
      self.activeCell += 1
      print(self.activeCell)
    elif (event.key=='enter'):
      print(self.cellCount)
      nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
      plt.bar(nums, self.cellCount)
      plt.show()
    else:
      print('Sum: ' + str(sum(self.cellCount)))

      
plot = clickerCounter()



