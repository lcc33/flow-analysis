generateColorPalette <- function(alpha){
  paletteOutput <- c()
  fileName <- '/home/lauren/Documents/research/cell-counting/colorPalette.csv'
  palette <- read.csv(fileName, header=TRUE)
  palette$r <- palette$r/255
  palette$g <- palette$g/255
  palette$b <- palette$b/255
  for (colorRow in c(1:nrow(palette))){
    if (alpha<1.01){
      color <- rgb(palette[colorRow,]$r, palette[colorRow,]$g, palette[colorRow,]$b, alpha=alpha)
    } else{
      colorScaler <- 2 - alpha 
      color <- rgb(palette[colorRow,]$r*colorScaler, palette[colorRow,]$g*colorScaler, palette[colorRow,]$b*colorScaler, alpha=1)
    }
    paletteOutput <- c(paletteOutput, color)
  }
  
  return(paletteOutput)
}
