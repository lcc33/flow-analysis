library(matrixStats)

extract_total_cells <- function(file_name){
  data <- read.csv(file=file_name, header=TRUE, sep=',')
  cell_count <- as.numeric(data[,3])
  doublet_count <- as.numeric(data[,4])
  total_count <- cell_count + doublet_count
  return(total_count)  
}

extract_all_samples <- function(file_names, folder_name, dilution_factor){
  sample_size = length(file_names)
  cells_total <- matrix(ncol=sample_size, nrow=5)
  for (ii in c(1:sample_size)){
    file_name = paste(folder_name, file_names[ii], sep="")
    cells_total[,ii] <- extract_total_cells(file_name)*dilution_factor[ii]
  }
  return(cells_total)
}

plot_error <- function(cells_total, color, time){
  sample_size <- dim(cells_total)[2]
  total_count_avg <- rowMeans(cells_total)
  print(total_count_avg)
  total_count_stdev <- rowSds(cells_total)
  total_count_error <- total_count_stdev*1.96/sqrt(sample_size)
  arrows(time, total_count_avg-total_count_error, time, total_count_avg+total_count_error,col=color, angle=90, code=3)  
  par(new=TRUE)
}

plot_cell_counts <- function(cells_total_30deg, cells_total_42deg, title){
  sample_size = dim(cells_total_30deg)[2]
  time <- c(0, 15, 30, 60, 90)
  time_42deg <- time + 0.5
  color_30deg <- 2
  color_42deg <- 5
  max_value <- max(cells_total_30deg, cells_total_42deg) 
  for (ii in c(1:sample_size)){
    plot(time, cells_total_30deg[,ii], col=color_30deg, xlab="Time (min)", ylab="Relative Population Size", main=title, ylim=c(0, max_value))
    par(new=TRUE)
    points(time_42deg, cells_total_42deg[,ii], col=color_42deg)
    par(new=TRUE)
  }
  plot_error(cells_total_30deg, color_30deg, time)
  plot_error(cells_total_42deg, color_42deg, time_42deg)

  legend("topleft", c("30 degrees", "42 degrees"), bty="n",pch=c(1, 1), col=c(color_30deg, color_42deg))
  par(new=FALSE)
}

export_total_counts <- function(time, cells_total_30deg, cells_total_42deg, file_name){
  cells_total_30deg <- rowSums(cells_total_30deg)
  cells_total_42deg <- rowSums(cells_total_42deg)
  data <-data.frame("time"=time, "cells_30deg"=cells_total_30deg, "cells_42deg"=cells_total_42deg)
  write.csv(data, file_name, row.names=FALSE)
}

folder_name = "4-24-19/"
file_names_30deg <- c("results_30deg_A.csv","results_30deg_B.csv")
file_names_42deg <- c("results_42deg_A.csv","results_42deg_B.csv")

dilution_factor = c(1, 1)
cells_total_30deg <- extract_all_samples(file_names_30deg, folder_name, dilution_factor)
cells_total_42deg <- extract_all_samples(file_names_42deg, folder_name, dilution_factor)

time <- c(0, 15, 30, 60, 90)
pdf(paste(folder_name,"results.pdf", sep=""))
plot_cell_counts(cells_total_30deg, cells_total_42deg, folder_name)
dev.off()

export_total_counts(time, cells_total_30deg, cells_total_42deg, paste(folder_name,"results_total_flow.csv", sep=""))