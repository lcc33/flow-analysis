library(matrixStats)
library(plotrix)
library(ggplot2)

read_csv_column <- function(file_name, column_number){
  data <- read.csv(file=file_name, header=TRUE, sep=',')
  column_data <- as.numeric(data[,column_number])
  return(column_data)
}

read_flow_data <- function(folder, column, samples, dilution_factor, time){
  n <- length(samples)  
  cells <- matrix(nrow=length(time), ncold=n*2)
  for (ii in c(1:n)){
    file_name <- paste(folder, "/results_30deg", samples[ii],".csv",sep='')
    cells[,ii] <- read_csv_column(file_name, column) * dilution_factor[ii]
    file_name <- paste(folder, "/results_42deg", samples[ii],".csv",sep='')
    cells[,ii+n] <- read_csv_column(file_name, column) * dilution_factor[ii]
  }
  return(cells)
}

read_cfu_data <- function(folder, time){
  file_name <- paste(folder, "/results_cfu.csv", sep='')
  cells_total <- matrix(nrow=length(time), ncol=4)
  for (ii in c(1:4)){
    cells_total[,ii] <- read_csv_column(file_name, ii+1)
  }
  return(cells_total)
}

plot_error <- function(cells_total, color, time){
  sample_size <- dim(cells_total)[2]
  total_count_avg <- rowMeans(cells_total)
  total_count_stdev <- rowSds(cells_total)
  if (total_count_avg[1]==1){
    total_count_stdev[1] <- 0.01
  }
  total_count_error <- total_count_stdev*1.96/sqrt(sample_size)
  width = 1.5
  xleft <- time-width
  xright <- time+width
  ybottom <- total_count_avg-total_count_error
  ytop <- total_count_avg+total_count_error
  color = paste(color, "50", sep="")
  rect(xleft, ybottom, xright, ytop, col=color, border=NA, density=50)  
  par(new=TRUE)
}

plot_cell_counts <- function(cells_total_30deg, cells_total_42deg, title, sample_names, ylabel, time){
  sample_size <- dim(cells_total_30deg)[2]
  time_42deg <- time + 0.5
  color_30deg <- "#0577B1"
  color_42deg <- "#C84E00"
  max_value <- max(cells_total_30deg, cells_total_42deg) 
  shapes <- c(0, 1, 2, 3, 4, 5, 6, 7, 8)
  for (ii in c(1:sample_size)){    
    plot(time, cells_total_30deg[,ii], pch=shapes[ii], cex=1.5, col=color_30deg, xlab="Time (min)", ylab=ylabel, main=title, ylim=c(0, max_value))
    par(new=TRUE)
    points(time_42deg, cells_total_42deg[,ii], pch=shapes[ii], col=color_42deg, cex=1.5)
    par(new=TRUE)
  }
  plot_error(cells_total_30deg, color_30deg, time)
  plot_error(cells_total_42deg, color_42deg, time_42deg)
  legend("topleft", c("30 degrees", "42 degrees"), bty="n",pch=15, col=c(color_30deg, color_42deg))
  legend("bottomright", sample_names, bty="n",pch=shapes)
  par(new=FALSE)
}

sum_cells <- function(cells_total, n){
  cells_total <- remove_outliers_two_samples(cells_total, n)
  cells_30deg <- rowSums(cells_total[,1:n])/sum(cells_total[1,1:n])
  cells_42deg <- rowSums(cells_total[,(n+1):(n*2)])/sum(cells_total[1,(n+1):(n*2)])
  results <- list("cells_30deg"=cells_30deg, "cells_42deg"=cells_42deg)
  return(results)
}

remove_outliers <- function(cells){
  time_points <- dim(cells)[1]
  for (ii in c(1:time_points)){
    row_mean <- mean(cells[ii,])
    print(row_mean)
    row_std <- sd(cells[ii,])
    print(row_std)
    ind_outliers <- which(cells[ii,]>row_mean+2*row_std || cells[ii,]<row_mean-2*row_std)
    ind <- which(cells[ii,]<row_mean+2*row_std && cells[ii,]>row_mean-2*row_std)
    row_mean <- mean(cells[ii, ind])
    cells[ii,ind_outliers] <- row_mean
  }
  return(cells)
}

remove_outliers_two_samples <- function(cells, n){
  if (n>2){
    total_samples <- dim(cells)[2]
    for (jj in c(1:2)){
      cells[,(jj*2-1):(jj*2+jj)] <- remove_outliers(cells[,(jj*2-1):(jj*2+jj)])
    }
  }
  return(cells)
}

plot_both_methods <- function(folder, samples, dilution_factor, time){
  print(folder)
  n <- length(samples)
  cells_total_flow <- read_flow_data(folder, 3, samples, dilution_factor, time) + read_flow_data(folder, 4, samples, dilution_factor, time)
  cells_total_flow <- remove_outliers_two_samples(cells_total_flow, n)
  print(cells_total_flow)
  plot_cell_counts(cells_total_flow[,1:n], cells_total_flow[,(n+1):(n*2)], paste("Flow:", folder), samples, "Total Cell Count", time)
  
  file_name = paste(folder, "/results_cfu.csv", sep='')
  
  if (file.exists(file_name)){
    cells_30deg <- matrix(0L, nrow=length(time), ncol=2)
    cells_42deg <- matrix(0L, nrow=length(time), ncol=2)
    
    n <- 2
    cells_total_cfu <- read_cfu_data(folder, time)
    plot_cell_counts(cells_total_cfu[,1:n], cells_total_cfu[,(n+1):(n*2)], paste("CFU Counting:", folder), samples[1:2], "CFU Count", time)
    results_cfu <- sum_cells(cells_total_cfu, n)
    cells_30deg[,2] <- results_cfu$cells_30deg
    cells_42deg[,2] <- results_cfu$cells_42deg

    n <- length(samples)
    cells_single_flow <- read_flow_data(folder, 3, samples, dilution_factor, time)
    results_flow <- sum_cells(cells_single_flow, n)
    cells_30deg[,1] <- results_flow$cells_30deg
    cells_42deg[,1] <- results_flow$cells_42deg
    
    plot_cell_counts(cells_30deg, cells_42deg, paste("Method Comparison:", folder), c("Flow", "CFU Counting"), "Short Cell Proportion", time)
  }
  plot.new()
  results <- list("cells_30deg_flow"=cells_30deg[,1], "cells_42deg_flow"=cells_42deg[,1],"cells_30deg_cfu"=cells_30deg[,2], "cells_42deg_cfu"=cells_42deg[,2])
  return(results)
}

pdf("results_overall.pdf", 7, 10)
par(mfrow=c(2,1))
time <- c(0, 15, 30, 60, 90)

cells_30deg_flow_overall <- matrix(0L, nrow=5, ncol=4)
cells_42deg_flow_overall <- matrix(0L, nrow=5, ncol=4)
cells_30deg_cfu_overall <- matrix(0L, nrow=5, ncol=4)
cells_42deg_cfu_overall <- matrix(0L, nrow=5, ncol=4)

folder <- "2019/4-22-19"
samples <- c("_high", "_low")
dilution_factor <- c(1, 10)
results <- plot_both_methods(folder, samples, dilution_factor, time)
cells_30deg_flow_overall[,1] <- results$cells_30deg_flow
cells_42deg_flow_overall[,1] <- results$cells_42deg_flow
cells_30deg_cfu_overall[,1] <- results$cells_30deg_cfu
cells_42deg_cfu_overall[,1] <- results$cells_42deg_cfu


folder <- "4-24-19"
samples <- c("_a", "_b")
dilution_factor <- c(1, 1)
results <- plot_both_methods(folder, samples, dilution_factor, time)
cells_30deg_flow_overall[,2] <- results$cells_30deg_flow
cells_42deg_flow_overall[,2] <- results$cells_42deg_flow
cells_30deg_cfu_overall[,2] <- results$cells_30deg_cfu
cells_42deg_cfu_overall[,2] <- results$cells_42deg_cfu


folder <- "5-1-19"
samples <- c("_", "_")
dilution_factor <- c(1, 1)
results <- plot_both_methods(folder, samples, dilution_factor, time)
cells_30deg_flow_overall[,3] <- results$cells_30deg_flow
cells_42deg_flow_overall[,3] <- results$cells_42deg_flow
cells_30deg_cfu_overall[,3] <- results$cells_30deg_cfu
cells_42deg_cfu_overall[,3] <- results$cells_42deg_cfu


folder <- "5-3-19"
samples <- c("_a", "_b", "_c", "_d", "_e")
dilution_factor <- c(1, 1, 1, 1, 1)
time <- c(0, 15, 30, 60)
results <- plot_both_methods(folder, samples, dilution_factor, time)

cells_30deg_flow_overall[1:4,4] <- results$cells_30deg_flow
cells_42deg_flow_overall[1:4,4] <- results$cells_42deg_flow
cells_30deg_cfu_overall[1:4,4] <- results$cells_30deg_cfu
cells_42deg_cfu_overall[1:4,4] <- results$cells_42deg_cfu

cells_30deg_flow_overall[5,4] <- mean(cells_30deg_flow_overall[1:4,4])
cells_42deg_flow_overall[5,4] <- mean(cells_42deg_flow_overall[1:4,4])
cells_30deg_cfu_overall[5,4] <- mean(cells_30deg_cfu_overall[1:4,4])
cells_42deg_cfu_overall[5,4] <- mean(cells_42deg_cfu_overall[1:4,4])


cells_30deg_flow_overall <- remove_outliers(cells_30deg_flow_overall)
cells_42deg_flow_overall <- remove_outliers(cells_42deg_flow_overall)
cells_30deg_cfu_overall <- remove_outliers(cells_30deg_cfu_overall)
cells_42deg_cfu_overall <- remove_outliers(cells_42deg_cfu_overall)

sample_names <- c("1", "2", "3", "4")
time <- c(0, 15, 30, 60, 90)
plot_cell_counts(cells_30deg_flow_overall, cells_42deg_flow_overall, "flow experiments", sample_names, "Proportion", time)
plot_cell_counts(cells_30deg_cfu_overall, cells_42deg_cfu_overall, "cfu experiments", sample_names, "Proportion", time)

cells_30deg_overall <- (cells_30deg_flow_overall + cells_30deg_cfu_overall)/2
cells_42deg_overall <- (cells_42deg_flow_overall + cells_42deg_cfu_overall)/2

plot_cell_counts(cells_30deg_overall, cells_42deg_overall, "all experiments", sample_names, "Proportion", time)


dev.off()

