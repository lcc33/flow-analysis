import numpy as np
import math
from scipy import signal
import matplotlib.pyplot as plt

def cellFinder(plate, colonyRadius, sensitivity):
  cell_average = createCell(colonyRadius)
  cell_corr = signal.correlate2d(plate, cell_average, mode='same', boundary='symm')
  cell_corr_original = cell_corr
  cell_corr_mean = np.mean(cell_corr)
  center = int(plate.shape[0]/2)
  radius_cutoff = int(plate.shape[0]/2)-2*colonyRadius-100
  cells = []
  neighbhorhood_width = colonyRadius*2
  plate_boundary = 200
  for ii in range(1000):
    ind = np.unravel_index(np.argmax(cell_corr), cell_corr.shape)
    if circleCheck(ind, [center, center], radius_cutoff):
      neighborhood = plate[ind[0]-neighbhorhood_width:ind[0]+neighbhorhood_width, ind[1]-neighbhorhood_width:ind[1]+neighbhorhood_width]
      neighborhood_mean = np.mean(neighborhood)
      if (plate[ind]>sensitivity*neighborhood_mean):
        cells.append(ind)
    cell_corr[ind[0]-colonyRadius:ind[0]+colonyRadius, ind[1]-colonyRadius:ind[1]+colonyRadius] = 0
  return cells

def circleCheck(pointToCheck, centerPoint, radiusCutoff):
  radius = (((pointToCheck[1]-centerPoint[1])**2 + (pointToCheck[0]-centerPoint[0])**2))**(0.5)
  if (radius<radiusCutoff):
    result = True
  else:
    result = False
  return result

def countCells(cells):
  cellCount = 0
  for ii in range(len(cells)):
    if (cells[ii][0]>0) & (cells[ii][1]>0):
      cellCount += 1
  return cellCount
  print('Cells marked: ' + str(cellCount))

def preprocessImage(plate, boundaries):
  plate = np.sum(plate, axis=2)
  plate = cropCircle(boundaries, plate)
  center = int(plate.shape[0]/2)
  circle_mask = circleMask(center-20, plate.shape[0])
  plate = np.multiply(plate, circle_mask)
  plate = np.divide(plate, np.max(plate))
  plate_mean = np.mean(plate)
  circle_mask = np.subtract(circle_mask, 2)
  circle_mask = np.abs(circle_mask)
  circle_mask = np.multiply(circle_mask, plate_mean)
  plate = np.add(plate, circle_mask)
  plate = np.pad(plate, (50, 50), 'mean')
  return plate

def createCell(colonyRadius):
  cell_average = np.zeros((colonyRadius*2, colonyRadius*2))
  colony_width = colonyRadius*2
  while (colonyRadius > 0):
    mask = circleMask(colonyRadius, colony_width)
    mask = np.multiply(mask, 0.1)
    cell_average = np.add(cell_average, mask)
    colonyRadius = colonyRadius - 2
  return(cell_average)

def circleMask(radius, maskWidth):
  mask = np.zeros((maskWidth, maskWidth))
  x = np.arange(0, maskWidth)
  y = np.arange(0, maskWidth)
  cx = int(maskWidth/2)
  cy = int(maskWidth/2)
  ind = (x[np.newaxis,:]-cx)**2 + (y[:,np.newaxis]-cy)**2 < radius**2
  mask[ind] = 1
  return mask

def cropCircle(boundaries, plate):
  totalPoints = len(boundaries)
  xvalues = np.zeros(totalPoints)
  yvalues = np.zeros(totalPoints)
  for ii in range(totalPoints):
    xvalues[ii] = boundaries[ii][0]
    yvalues[ii] = boundaries[ii][1]
  center = int(sum(xvalues)/totalPoints), int(sum(yvalues)/totalPoints)
  rightBound = xvalues[np.argmax(xvalues)]
  leftBound = xvalues[np.argmin(xvalues)]
  radius = int((rightBound - leftBound)/2)
  plate = plate[center[1]-radius:center[1]+radius, center[0]-radius:center[0]+radius]
  return plate
