import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import math
import sys
import cfuCounterLib as cfu
import os
from os import listdir
from os.path import isfile, join
from tkinter import filedialog
from tkinter import *

class cfuCounter: 
  def __init__(self):
    sys.setrecursionlimit(3000)
    fileName =  self.openFile()
    self.plate = mpimg.imread(fileName) 
    self.fig = plt.figure()
    cid = self.fig.canvas.mpl_connect('button_press_event', self.onClick)
    cid = self.fig.canvas.mpl_connect('key_press_event', self.onKey)
    self.cropped = False
    self.boundaries = []
    self.cfuRadius = 40
    self.sensitivity = 1.2
    self.cells = []
    self.plotAllCells()

  def openFile(self):
    path = os.path.dirname(os.path.realpath(__file__))
    root = Tk()
    root.update()
    fileName =  filedialog.askopenfilenames(initialdir = path,title = "Select file")
    root.destroy()
    print(fileName[0])
    return fileName[0]

  def plotAllCells(self):
    plt.cla()
    plt.imshow(self.plate)
    for ii in range(len(self.cells)):
      if (self.cells[ii][0]>0) & (self.cells[ii][1]>0):
        coord1 = self.plate.shape[1] - self.cells[ii][1]
        plt.scatter(self.cells[ii][1], self.cells[ii][0], s=1, c='red')
    plt.show()

  def plotSingleCell(self):
    coord1 = self.plate.shape[1] - self.cells[-1][1]
    plt.scatter(self.cells[-1][1], self.cells[-1][0], s=1, c='red')
    plt.show()
  
  def onClick(self, event):
    if (self.cropped):
      self.editCellLocations(event)
    else:
      self.editPlateBoundaries(event)
  
  def onKey(self, event):
    if (event.key=='enter'):
      self.cropped = True
      self.startAnalysis()
      cellCount = cfu.countCells(self.cells)
      print('Cells marked: ' + str(cellCount))
    
  def startAnalysis(self):
    self.plate = cfu.preprocessImage(self.plate, self.boundaries)
    print('Identifying CFUs...')
    self.cells = cfu.cellFinder(self.plate, self.cfuRadius, self.sensitivity)
    print('Cells found...')
    self.plotAllCells()
  
  def editPlateBoundaries(self, event):
    self.boundaries.append([event.xdata, event.ydata])
  
  def editCellLocations(self, event):
    totalCells = len(self.cells)
    self.clickLocation = [event.xdata, event.ydata]
    self.checkClickLocation()
    if (totalCells==len(self.cells)):
      self.plotAllCells()
    else:
      self.plotSingleCell()
  
  def checkClickLocation(self):
    closePoints = 0
    for ii in range(len(self.cells)):
      distanceBetween = math.sqrt((self.cells[ii][1]-self.clickLocation[0])**2 + (self.cells[ii][0]-self.clickLocation[1])**2)
      if (distanceBetween < 20):
        closePoints += 1
        self.cells[ii] = [0, 0]
    if (closePoints<1):
      ycoor = int(self.clickLocation[1])
      xcoor = int(self.clickLocation[0])
      self.cells.append([ycoor, xcoor])

counter = cfuCounter()
